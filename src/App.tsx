import "./app/asstes/css/index.scss";
import { Login } from "./app/views";
import { AuthRoutes } from "./routes/authRoutes";
import { MainRoutes } from "./routes/mainRoutes";
import { useAuthContext } from "./shared/contexts";

function App() {
  const authContext = useAuthContext();

  return (
    <div className="App">
      {authContext.isAuthenticated ? <MainRoutes /> : <AuthRoutes />}
      {/* <MainRoutes /> */}
    </div>
  );
}

export default App;
