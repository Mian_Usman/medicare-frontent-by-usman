import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Login } from "../../app/views";
import { ForgetPasswordVarification } from "../../app/views/LoginSubPages";
import { AuthLayout } from "../../layouts/AuthLayout";

export const AuthRoutes = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AuthLayout />}>
            <Route path="login" element={<Login />} />
            <Route
              path="forget-password-verification"
              element={<ForgetPasswordVarification />}
            />
            <Route path="/" element={<Navigate to={"/login"} />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};
