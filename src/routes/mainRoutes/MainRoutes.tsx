import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { VideoCallings } from "../../app/components/chatsComponents/VideoCallings";

import {
  Chat,
  Clinics,
  Dashboard,
  Login,
  NewClinic,
  NotFoundPage,
  Organization,
  Setting,
  Subscription,
} from "../../app/views";
import { ChatsConversation } from "../../app/views/ChatsSubPages";
import {
  CreateNewOrganization,
  DetailsOranizationData,
} from "../../app/views/OraganizationSubPages";
import { SubscriptionPackages } from "../../app/components/SubscriptionComponents";
import { MainLayout } from "../../layouts/MainLayout";
import { DeleteClinicModal } from "../../app/components/ClinicsComponets";
import { ForgetPasswordVarification } from "../../app/views/LoginSubPages";
import {
  AddNewPatient,
  AddNewStaff,
  Patient,
  Staff,
  SubDashboard,
  SubSettings,
} from "../../app/views/SubAdmin";

export const MainRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* <Route path="/" element={<DeleteClinicModal />}> */}
        <Route path="/" element={<MainLayout />}>
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/clinics" element={<Clinics />} />
          <Route path="/subscription" element={<Subscription />} />
          <Route path="/organization" element={<Organization />} />
          <Route path="/chat" element={<Chat />} />
          <Route path="/setting" element={<Setting />} />
          <Route path="/new-clinic" element={<NewClinic />} />
          <Route path="/chats-conversation" element={<ChatsConversation />} />
          <Route
            path="/create-new-organization"
            element={<CreateNewOrganization />}
          />
          <Route path="/video-calling" element={<VideoCallings />} />
          <Route
            path="/details-organization-data"
            element={<DetailsOranizationData />}
          />
          <Route path="/staff" element={<Staff />} />
          <Route path="/patient" element={<Patient />} />
          <Route path="/add-new-patient" element={<AddNewPatient />} />
          <Route path="/add-new-staff" element={<AddNewStaff />} />
          <Route path="/sub-setting" element={<SubSettings />} />

          {/* SUBADMIN_ROUTES */}

          <Route path="/sub-dashboard" element={<SubDashboard />} />

          <Route path="/" element={<Navigate to={"/sub-dashboard"} />} />
        </Route>
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
};
