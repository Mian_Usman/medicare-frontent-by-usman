import { IsNotEmpty } from "class-validator";

export enum Staff {
  doctor = "doctor",
  nurse = "nurse",
  other = "other",
  management = "management",
}

export class AddNewStaffValidator {
  @IsNotEmpty({ message: "Must enter your name" })
  firstName: string;

  @IsNotEmpty({ message: "Must enter your name" })
  lastName: string;
  @IsNotEmpty({ message: "Must enter your name" })
  email: string;

  @IsNotEmpty({ message: "Must enter your name" })
  phoneNumber: string;
  @IsNotEmpty({ message: "Must enter your name" })
  licence: string;

  // @IsNotEmpty({ message: "Must enter your name" })
  // profile: string;

  @IsNotEmpty({ message: "Must enter your name" })
  password: string;

  @IsNotEmpty({ message: "Must enter your name" })
  staff: Staff[];
}
