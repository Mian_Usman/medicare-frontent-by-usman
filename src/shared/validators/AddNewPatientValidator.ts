import { IsNotEmpty } from "class-validator";

export class AddNewPatientValidator {
  name: string;
  email: string;
  address1: string;
  address2: string;
  phoneNumber: string;
  // gender: string;
  dob: string;
}
