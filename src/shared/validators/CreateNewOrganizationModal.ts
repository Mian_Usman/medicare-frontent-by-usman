import { IsNotEmpty, IsString } from "class-validator";

export class CreateNewOrganzationModalValidator {
  @IsNotEmpty({ message: "it must be fill" })
  name: string;

  @IsString()
  @IsNotEmpty({ message: "it must be fill" })
  description: string;
}
