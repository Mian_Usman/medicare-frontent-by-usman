import { IsNotEmpty } from "class-validator";

export class AddNewClinicValidator {
  @IsNotEmpty({ message: "Must enter your name" })
  name: string;

  @IsNotEmpty({ message: "Must enter your email address" })
  email: string;

  @IsNotEmpty({ message: "Must enter your phone number" })
  phoneNumber: string;

  @IsNotEmpty({ message: "Must enter your subscription plan" })
  plan: string;

  @IsNotEmpty({ message: "Must enter your address" })
  address1: string;

  address2: string;

  @IsNotEmpty({ message: "Must enter admin name" })
  adminFirstName: string;

  @IsNotEmpty({ message: "Must enter admin last name" })
  adminLastName: string;

  @IsNotEmpty({ message: "Must enter admin email address" })
  adminEmail: string;

  @IsNotEmpty({ message: "Must enter admin phone number" })
  adminPh: string;

  @IsNotEmpty({ message: "Must enter your password" })
  password: string;

  @IsNotEmpty({ message: "Must enter your organization name" })
  organization: string;
}
