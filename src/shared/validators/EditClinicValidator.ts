import { IsNotEmpty } from "class-validator";

export class EditClinicValidator {
  @IsNotEmpty({ message: "Must enter your name" })
  name: string;

  @IsNotEmpty({ message: "Must enter your email" })
  email: string;

  @IsNotEmpty({ message: "Must enter your number" })
  phoneNumber: string;

  @IsNotEmpty({ message: "Must enter your address" })
  address1: string;

  @IsNotEmpty({ message: "Must enter your address2" })
  address2: string;

  @IsNotEmpty({ message: "Must enter your plan" })
  plan: string;

  @IsNotEmpty({ message: "Must enter your organization" })
  organization: string;
}
