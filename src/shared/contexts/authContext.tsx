import React, { createContext, PureComponent, useContext } from "react";
import { Cookies } from "../../shared/utility";
import { UsersInterface } from "../interfaces";

interface State {
  user: UsersInterface;
  isAuthenticated: boolean;
}

interface Context extends State {
  updateUser(user: UsersInterface): void;
  getLatestUser(): void;
}

export const AuthContext = createContext<Context>(null);
export const useAuthContext = () => useContext(AuthContext);
export class AuthContextProvider extends PureComponent<any, State> {
  state: State = {
    user: null,
    isAuthenticated: false,
  };
  componentDidMount() {
    const user = Cookies.get("current-user");
    if (user) {
      this.setState({ user, isAuthenticated: true });
      this.getLatestUser();
    }
  }

  getLatestUser(): void {}

  updateUser(user: UsersInterface): void {
    if (user) {
      Cookies.set("current-user", user);
      if (user.authToken) {
        Cookies.set("access-token", user.authToken);
      }
      this.setState({ user, isAuthenticated: true });
    } else {
      Cookies.remove("current-user");
      Cookies.remove("access-token");
      this.setState({ user, isAuthenticated: false });
    }
  }

  render() {
    const context: Context = {
      ...this.state,
      updateUser: this.updateUser.bind(this),
      getLatestUser: this.getLatestUser.bind(this),
    };
    return (
      <AuthContext.Provider value={context}>
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}
