export interface AddNewStaffInterface {
  id: string;
  name: string;
  serialNumber: number;
  email: string;
  address1: string;
  address2: string;
  plan: string;
  emailVerified: boolean;
  phoneNumber: string;
  phoneNumberVerified: boolean;
  orgnizations: string[];
  verifiedPublicHealth: string[];
  profile: string;
  paypal: boolean;
  organization: string;
  admin: string;
}
