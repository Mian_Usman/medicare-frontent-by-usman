export interface ListNewOrganizationInterface {
  id: string;
  name: string;
  description: string;
  clinics: number;
  doctors: number;
  others: number;
}

export interface DeleteOrganizationInterface {
  name: string;
  description: string;
  clinics: number;
  doctors: number;
  others: number;
}
