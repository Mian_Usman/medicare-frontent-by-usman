import { Organization } from "./../../app/views/Organization";
export enum UserRoles {
  ClinicAmin = "clinicAmin",
  SuperAdmin = "superAdmin",
  OtherStaff = "otherStaff",
}
export enum Gender {
  Male = "male",
  Female = "female",
}
export enum Staff {
  doctor = "doctor",
  nurse = "nurse",
  Other = "other",
  management = "management",
}

// export enum Clinic {
//     name: string;
//     serialNumber: string;
//     email: string;
//     address1: string;
//     address2: string;
//     plan: string;
//     emailVerified: falsestring;
//     phoneNumber: string;
//     phoneNumberVerified: falsestring;
//     orgnizations: []string;
//     verifiedPublicHealth: []string;
//     profile: string;
//     paypal: falsestring;
//     organization: string;
//     admin: string;
//   }

export interface UsersInterface {
  firstname: string;
  lastName: string;
  phoneNumber: string;
  gender: Gender[];
  address1: string;
  address2: string;
  emailVerified: false;
  phoneNumberVerified: false;
  profile: string;
  roles: UserRoles[];
  licence: string;
  licenceExpiry: string;
  staff: Staff[];
  organization: string[];
  newPasswordReset: boolean;
  authToken: string;
}
