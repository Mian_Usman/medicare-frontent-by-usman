export interface CreateNewOraganizationModal {
  name: string;
  description: string;
  clinics: number;
  doctors: number;
  others: number;
}
