export interface VacancyInterface {
  title: string;
  description: string;
  industry: string;
  location: string;
  remoteWorking: string;
  contractType: string;
  salary: NumberRange;
  tags: string[];
  benifits: string[];
  questions: string[];
}

export interface NumberRange {
  start: string;
  end: string;
}
