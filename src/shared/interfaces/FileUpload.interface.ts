export interface FileUploadInterface {
  filename: string;
  path: string;
}
