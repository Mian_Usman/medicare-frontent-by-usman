export class AddNewPateintInterface {
  name: string;
  email: string;
  address1: string;
  address2: string;
  emailVerified: false;
  phoneNumber: string;
  phoneNumberVerified: false;
  clinic: string;
  profile: string;
  verified: true;
  verifiedBy: string;
  verifiedStatement: string;
  attachments?: [];
  publicId: string;
  gender: string;
  images?: [];
  dob: string;
  iHealthID?: string;
}
