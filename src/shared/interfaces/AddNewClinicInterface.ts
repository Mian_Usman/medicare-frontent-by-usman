export interface AddNewClinicInterface {
  id: string;
  name: string;
  serialNumber: 0;
  email: string;
  address1: string;
  address2: string;
  adminFirstName: string;
  adminLastName: string;
  plan: string;
  emailVerified: false;
  phoneNumber: string;
  phoneNumberVerified: false;
  orgnizations: string[];
  verifiedPublicHealth: string[];
  profile: string;
  paypal: false;
  organization: string;
  admin: string;
}
