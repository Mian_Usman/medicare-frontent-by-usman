import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../api";

export const OrganizationListThunk = createAsyncThunk(
  "Organization-List-thunk",
  async (data, thunkApi) => {
    try {
      const response = await API.Organization.list();
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);

export const OrganizationDeleteThunk = createAsyncThunk(
  "Organization-Delete-thunk",
  async (id: string, thunkApi) => {
    try {
      const response = await API.Organization.deleteOrganizationById(id);
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
