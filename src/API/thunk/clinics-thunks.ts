import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../api";

export const ClinicsListThunk = createAsyncThunk(
  "Clinics-thunk",
  async (data, thunkApi) => {
    try {
      const response = await API.Clinics.list();
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);

export const DeleteClinicsThunk = createAsyncThunk(
  "Delete-Clinic-thunk",
  async (id: string, thunkApi) => {
    //id must have pass here in case of delete of edit that API
    try {
      const response = await API.Clinics.deleteClinicById(id);
      return response.data;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.response);
    }
  }
);
