import { createSlice } from "@reduxjs/toolkit";
import { ListNewOrganizationInterface } from "../../shared/interfaces";
import {
  OrganizationDeleteThunk,
  OrganizationListThunk,
} from "../thunk/Organization-thunk";

interface OrganizationState {
  data: ListNewOrganizationInterface[];
  loading: boolean;
  error: any;
}

const initialState: OrganizationState = {
  data: [],
  loading: false,
  error: null,
};
export const OrganizationSlice = createSlice({
  name: "Organization-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(OrganizationListThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(OrganizationListThunk.fulfilled, (state, { payload }) => {
        state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(OrganizationListThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });

    // delete

    builder
      .addCase(OrganizationDeleteThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(OrganizationDeleteThunk.fulfilled, (state, { payload }) => {
        // state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(OrganizationDeleteThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
