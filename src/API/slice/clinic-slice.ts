import { AddNewClinicInterface } from "./../../shared/interfaces/AddNewClinicInterface";
import { createSlice } from "@reduxjs/toolkit";
import { ClinicsListThunk, DeleteClinicsThunk } from "../thunk";

interface clinicState {
  data: AddNewClinicInterface[];
  loading: boolean;
  error: any;
}

const initialState: clinicState = {
  data: [],
  loading: false,
  error: null,
};
export const ClinicSlice = createSlice({
  name: "clinics-slice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // List the Clinic
    builder
      .addCase(ClinicsListThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(ClinicsListThunk.fulfilled, (state, { payload }) => {
        state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(ClinicsListThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });

    // Delete

    builder
      .addCase(DeleteClinicsThunk.pending, (state) => {
        state.loading = !state.data.length;
        state.error = null;
      })
      .addCase(DeleteClinicsThunk.fulfilled, (state, { payload }) => {
        // state.data = payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(DeleteClinicsThunk.rejected, (state, action) => {
        if (action.payload) {
          state.error = action.payload;
        } else {
          state.error = action.error;
        }
        state.loading = false;
      });
  },
});
