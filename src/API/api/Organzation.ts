import {
  DeleteOrganizationInterface,
  ListNewOrganizationInterface,
} from "./../../shared/interfaces/create-new-organization";
import { CreateNewOrganzationModalValidator } from "./../../shared/validators/CreateNewOrganizationModal";
import { ForgetPasswordValidator } from "./../../shared/validators/forget-password-email";
import { UsersInterface } from "./../../shared/interfaces/UsersInteface";
import { AxiosPromise } from "axios";
import { LoginFormValidator } from "./../../shared/validators/loginFormValidator";
import { axiosInstance } from "./AxiosInstance";
import { CreateNewOraganizationModal } from "../../shared/interfaces";

export class Organization {
  static list(): AxiosPromise<ListNewOrganizationInterface[]> {
    return axiosInstance({
      url: "/organization/all",
      method: "get",
    });
  }

  static CreateNewOrganization(
    data: CreateNewOrganzationModalValidator
  ): AxiosPromise<CreateNewOraganizationModal> {
    return axiosInstance({
      url: "/organization/create",
      method: "post",
      data,
    });
  }

  static deleteOrganizationById(
    id: string
  ): AxiosPromise<CreateNewOraganizationModal> {
    return axiosInstance({
      url: "/organization",
      method: "delete",
      params: { organization: id },
    });
  }
}
