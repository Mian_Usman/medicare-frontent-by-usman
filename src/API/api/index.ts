import { Media } from "./Media";
import { Organization } from "./Organzation";
import { Clinics } from "./Clinics";
import { Auth } from "./Auth";
export const API = {
  Auth,
  Clinics,
  Organization,
  Media,
};
