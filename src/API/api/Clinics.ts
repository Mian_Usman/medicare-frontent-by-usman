import { AddNewPatientValidator } from "./../../shared/validators/AddNewPatientValidator";
import { AddNewPateintInterface } from "./../../shared/interfaces/AddNewPatient.interface";
import { AddNewClinicInterface } from "./../../shared/interfaces/AddNewClinicInterface";
import { AddNewClinicValidator } from "./../../shared/validators/AddNewClinicValidator";
import { AxiosPromise } from "axios";
import { axiosInstance } from "./AxiosInstance";
import { AddNewStaffValidator } from "../../shared/validators";
import { EditClinicValidator } from "../../shared/validators/EditClinicValidator";
import { ListNewOrganizationInterface } from "../../shared/interfaces";

export class Clinics {
  static create(
    data: AddNewClinicValidator
  ): AxiosPromise<AddNewClinicInterface> {
    return axiosInstance({
      url: "/clinic/create",
      method: "post",
      data,
    });
  }

  static list(): AxiosPromise<AddNewClinicInterface[]> {
    return axiosInstance({
      url: "/clinic/all",
      method: "get",
    });
  }

  static createNewStaffApi(
    id: string,
    data: AddNewStaffValidator
  ): AxiosPromise<AddNewClinicInterface> {
    return axiosInstance({
      url: "/clinic/addStaff",
      method: "post",
      data,
      params: { clinic: id },
    });
  }

  static deleteClinicById(
    id: string
  ): AxiosPromise<ListNewOrganizationInterface> {
    return axiosInstance({
      url: "/clinic/remove",
      method: "delete",
      params: { clinic: id },
    });
  }

  static updateClinicById(
    id: string,
    data: EditClinicValidator
  ): AxiosPromise<AddNewClinicInterface> {
    return axiosInstance({
      url: "/clinic",
      method: "patch",
      data,
      params: { clinic: id },
    });
  }

  //pateint

  static createNewPatient(
    data: AddNewPatientValidator
  ): AxiosPromise<AddNewPateintInterface> {
    return axiosInstance({
      url: "/clinic/patient",
      method: "post",
      data,
    });
  }
}
