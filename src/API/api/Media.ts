import { axiosInstance } from "./AxiosInstance";
import { AxiosPromise } from "axios";
import React from "react";
import { FileUploadInterface } from "../../shared/interfaces";

export class Media extends React.Component {
  static upload(
    file: File,
    onUploadProgress?: (progressEvent: ProgressEvent) => void
  ): AxiosPromise<FileUploadInterface> {
    const formData = new FormData();
    formData.set("file", file);
    return axiosInstance({
      url: "/media/upload",
      method: "post",
      data: formData,
      onUploadProgress,
    });
  }

  static getFileByName(filename: string): string {
    const url = new URL("http://localhost:3000/");
    url.pathname = `/media/${filename}`;
    return url.href;
  }
}
