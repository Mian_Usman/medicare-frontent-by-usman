import { ForgetPasswordValidator } from "../../shared/validators/forget-password-email";
import { UsersInterface } from "../../shared/interfaces/UsersInteface";
import { AxiosPromise } from "axios";
import { LoginFormValidator } from "../../shared/validators/loginFormValidator";
import { axiosInstance } from "./AxiosInstance";

export class Auth {
  static login(data: LoginFormValidator): AxiosPromise<UsersInterface> {
    return axiosInstance({
      url: "/auth/login",
      method: "post",
      data,
    });
  }

  static ForgetPasswordEmail(data: ForgetPasswordValidator) {
    return axiosInstance({
      url: "/auth/forgetPassword",
      method: "post",
      data,
    });
  }
}
