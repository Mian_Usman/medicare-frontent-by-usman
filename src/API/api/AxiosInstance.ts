import { Storage } from "./../../shared/utility/Storage";
import axios, { CancelTokenSource } from "axios";

export const axiosInstance = axios.create({
  baseURL: "http://150.230.140.18:4100/",
});

const pendingRequests: Record<string, CancelTokenSource> = {};
axiosInstance.interceptors.request.use((config) => {
  const token = Storage.get("access-token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  console.log(config.url);
  if (!!pendingRequests[config.url]) {
    pendingRequests[config.url].cancel("Duplicate Request");
  }
  const cancelTokenSource = axios.CancelToken.source();
  config.cancelToken = cancelTokenSource.token;
  pendingRequests[config.url] = cancelTokenSource;
  return config;
});
axiosInstance.interceptors.response.use(
  (response) => {
    delete pendingRequests[response.config.url];
    return response;
  },
  (error) => {
    if (!axios.isCancel(error)) {
      delete pendingRequests[error.config.url];
    } else {
      return Promise.reject({
        data: {},
        status: 408,
        statusText: "Request is cancel due to duplicate",
      });
    }
    if (error.response) {
      if (error.response.status === 401) {
        Storage.clear();
        window.location.reload();
      }
    }
    return Promise.reject(error);
  }
);
