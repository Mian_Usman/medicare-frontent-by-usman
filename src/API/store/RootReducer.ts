import { ClinicSlice } from "./../slice/clinic-slice";
import { OrganizationSlice } from "./../slice";
import { combineReducers } from "@reduxjs/toolkit";

export const rootReducer = combineReducers({
  organization: OrganizationSlice.reducer,
  clinic: ClinicSlice.reducer,
});
