import React from "react";
import ReactDOM from "react-dom/client";
import store from "./API/store";
import { Provider } from "react-redux";
import App from "./App";
import { AuthContextProvider } from "./shared/contexts";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
    <AuthContextProvider>
      <App />
    </AuthContextProvider>
  </Provider>
);
