import { useFormik } from "formik";
import { NavLink, useNavigate } from "react-router-dom";
import { API } from "../../API/api";
import { useAuthContext } from "../../shared/contexts";
import { FormikValidator } from "../../shared/utility";
import { LoginFormValidator } from "../../shared/validators/loginFormValidator";
import { FormikErrorMessage } from "../components";
const loginImg = require("../asstes/gallery/login-img.png");
const logo = require("../asstes/gallery/logo.png");

export const Login = () => {
  const Context = useAuthContext();
  const navigate = useNavigate();
  const LoginFormik = useFormik<LoginFormValidator>({
    initialValues: {
      identifier: "",
      password: "",
    },
    onSubmit: (values) => {
      API.Auth.login(values)
        .then((response) => {
          Context.updateUser(response.data);
          navigate("/sub-dashboard");
        })
        .catch((error) => {
          console.log(error);
        });
    },

    validate: FormikValidator.validator(LoginFormValidator),
  });

  return (
    <div className="main-container">
      <div className="white-container">
        <div className="login-img">
          <img src={loginImg} alt="login image" />
        </div>
      </div>
      <div className="green-container">
        <div className="login-form-div">
          <div className="login-form-container">
            <div className="logo">
              <img src={logo} alt="" />
            </div>
            <p>Login to go to your account</p>
            <div className="login-form">
              <form onSubmit={LoginFormik.handleSubmit}>
                <div className="input-div">
                  <label>E-mail</label>
                  <input
                    className="inputemail"
                    type="email"
                    name="identifier"
                    placeholder="example@gmai.com"
                    onChange={LoginFormik.handleChange}
                    onBlur={LoginFormik.handleBlur}
                  />
                  <FormikErrorMessage
                    formik={LoginFormik}
                    name="identifier"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>
                <div className="input-div">
                  <label>Password</label>
                  <input
                    className="inputemail"
                    type="password"
                    name="password"
                    id="password"
                    placeholder="Ener your password"
                    onChange={LoginFormik.handleChange}
                    onBlur={LoginFormik.handleBlur}
                  />
                  <FormikErrorMessage
                    formik={LoginFormik}
                    name="password"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>
                <div className="login-remember-forgot">
                  <span
                    style={{ fontSize: "17px", display: "flex", gap: "5px" }}
                  >
                    <input type="checkbox" name="checkbox" id="checkbox" />
                    Remember me
                  </span>
                  <NavLink to={"/forget-password-verification"}>
                    <div
                      style={{
                        fontSize: "17px",
                        cursor: "pointer",
                        textDecoration: "underl ine",
                      }}
                    >
                      forgot password?
                    </div>
                  </NavLink>
                </div>
                <div className="login-button">
                  <button type="submit">Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
