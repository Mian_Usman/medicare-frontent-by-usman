import React from "react";
const img = require("../asstes/gallery/settings-form-img.png");

export const Setting = () => {
  return (
    <div className="settings-main-container">
      <div className="settings-container">
        <h2 style={{ padding: "15px" }}>Settings</h2>
      </div>
      <div className="settings-white-board">
        <div className="settings-form-section">
          <div className="form-settings-image">
            <img src={img} alt="" />
          </div>
          <form action="">
            <div className="label-input">
              <label htmlFor="">Name</label>
              <input type="name" name="name" id="-name" placeholder=" Name" />
            </div>
            <div className="label-input">
              <label htmlFor=""> Email</label>
              <input
                type="email"
                name="-email"
                id="-email"
                placeholder=" Email"
              />
            </div>
            <div className="label-input">
              <label htmlFor=""> Phone Number</label>
              <input
                type="number"
                name="number"
                id="-number"
                placeholder=" Phone Number"
              />
            </div>
            <div className="label-input">
              <label htmlFor="">Password</label>
              <input
                type="password"
                name="password"
                id="password"
                placeholder="Password"
              />
            </div>

            <div className="settings-form-button">
              <button className="empty-light-button">Discard</button>
              <button className="solid-button" type="submit">
                Save Changes
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
