import React from "react";
import { NavLink } from "react-router-dom";
const notFount = require("../asstes/gallery/notfounderror.png");

export const NotFoundPage = () => {
  return (
    <div className="notfound-main-container">
      <div className="notfound-container">
        <img src={notFount} alt="not fount" />
        <p>Page Not Found</p>
        <NavLink to={"/"}>
          <button>Back To Home</button>
        </NavLink>
      </div>
    </div>
  );
};
