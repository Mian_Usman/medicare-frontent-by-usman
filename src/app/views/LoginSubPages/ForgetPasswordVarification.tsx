import { useFormik } from "formik";
import { API } from "../../../API/api";
import { FormikValidator } from "../../../shared/utility";
import { ForgetPasswordValidator } from "../../../shared/validators";
import img from "../../asstes/gallery/1forgetPasswordLock.svg";
import { FormikErrorMessage } from "../../components";

export const ForgetPasswordVarification = () => {
  const ForgetPasswordFormik = useFormik<ForgetPasswordValidator>({
    initialValues: {
      email: "",
    },
    onSubmit: (values) => {
      API.Auth.ForgetPasswordEmail(values)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    validate: FormikValidator.validator(ForgetPasswordValidator),
  });

  return (
    <div className="forget-password-background-container">
      <div className="forget-password-container">
        <div className="lock-img">
          <div className="round-img">
            <img src={img} alt="" />
          </div>
          <h4>Reset Password</h4>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore,
            natus.
          </p>
        </div>
        <form onSubmit={ForgetPasswordFormik.handleSubmit}>
          <div className="inputs">
            <div>
              <label htmlFor="">Email</label>
              <input
                type="email"
                name="email"
                id="email"
                placeholder="Enter your email address"
                onChange={ForgetPasswordFormik.handleChange}
                onBlur={ForgetPasswordFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={ForgetPasswordFormik}
                name="email"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            {/* <div>
            <label htmlFor="">Confirm Password</label>
            <input
              type="password"
              name="password"
              id="password"
              placeholder="Confirm Password"
            />
          </div> */}
            <button type="submit" className="reset-password-btn">
              Confirm
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
