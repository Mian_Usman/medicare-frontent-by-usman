import React from "react";
import { SubscriptionFee } from "../../components";
import { DashboardAppoinments } from "../../components/SubAdminComponents/dashboardComponents";
import Person from "../../asstes/gallery/1SubDashboardPerson.svg";
import WheelChair from "../../asstes/gallery/1SubWheelChairDashboard.svg";
import Calender from "../../asstes/gallery/1SubCalenderDashboard.svg";

export const SubDashboard = () => {
  return (
    <div className="dashboard-main-container">
      <div className="dashboard-container">
        <div className="top-three-cards">
          <div className="card1">
            <div className="img">
              <img src={Person} alt="" />
            </div>
            <div className="cards-data">
              <p>Staff Member</p>
              <div className="cards-h1">26</div>
            </div>
          </div>
          <div className="card1">
            <div className="img">
              <img src={WheelChair} alt="" />
            </div>
            <div className="cards-data">
              <p>Patients</p>
              <div className="cards-h1">2213</div>
            </div>
          </div>
          <div className="card1">
            <div className="img">
              <img src={Calender} alt="" />
            </div>
            <div className="cards-data">
              <p>Appointments</p>
              <div className="cards-h1">31</div>
            </div>
          </div>
        </div>

        {/*  */}
        <div className="blocks-subscription-section">
          <div className="dashboard-empty-blocks">
            <div className="block-1"></div>
            <div className="block-1"></div>
          </div>

          <div className="subscription-fee-section">
            <div style={{ padding: 10 }} className="request-button">
              <h3 style={{ fontSize: "22px", fontWeight: "bold" }}>
                Today Appointments
              </h3>
            </div>

            <DashboardAppoinments
              name="Patient: Hammad Bemar"
              desgination="Doctor: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <DashboardAppoinments
              name="Patient: Hammad Bemar"
              desgination="Doctor: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <DashboardAppoinments
              name="Patient: Hammad Bemar"
              desgination="Doctor: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <DashboardAppoinments
              name="Patient: Hammad Bemar"
              desgination="Doctor: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>

            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
