import { Button, Dropdown, FormControl, InputGroup } from "react-bootstrap";
import imag from "../../asstes/gallery/1threeDotsMenu.svg";
import { StaffTable } from "../../components/SubAdminComponents/StaffComponents";

export const Staff = () => {
  const clinicsDots = (
    <div className="dropdown-organization">
      <Dropdown className="header-dropdown-main">
        <Dropdown.Toggle className="header-dropdown transparents">
          <img src={imag} alt="" className="transparents" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="header-dropdown-menu">
          <Dropdown.Item className="header-dropdown-item">Edit</Dropdown.Item>
          <Dropdown.Item className="header-dropdown-item">Delete</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );

  const recievedbtn = <button className="recieved-button">Recieved</button>;
  const pendingbtn = <button className="pending-button">Pending</button>;

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Staff</h2>
      </div>
      <div className="clinic-white-board">
        <div className="seach-bar-select-menu">
          <div className="">
            <InputGroup className="">
              <FormControl
                className="Searchbaar-input"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="basic-addon2"
              />
              <Button className="searchbaar-button">
                <i className="fa fa-search"></i>
              </Button>
            </InputGroup>
          </div>

          <div className="staff-bar-select">
            <select name="Doctors" id="select" className="staff-select">
              <option value="pending">Doctors</option>
              <option value="recieved">Nurse</option>
              <option value="recieved">Others</option>
            </select>
          </div>
        </div>
        <div className="clinic-table">
          <table>
            <thead>
              <tr style={{ fontWeight: "700" }}>
                <th scope="col">No.</th>
                <th scope="col">Doctor Name</th>
                <th scope="col">Specialty </th>
                <th scope="col">Email</th>
                <th scope="col">Phone Number</th>
                <th scope="col">License Expiry</th>
              </tr>
            </thead>
            <tbody>
              <StaffTable
                Number="001"
                DoctorName="Hammad Clinic"
                speciality="Abdullah"
                Email="abdullah@gmail.com"
                PhoneNumber="Lahore"
                LicenceExpiry="+92 304 8181784"
                clinicDots={clinicsDots}
              />
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
