import { useFormik } from "formik";
import { stringify } from "querystring";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { API } from "../../../API/api";
import { useDispatch, useSelector } from "../../../API/store";
import { FormikValidator } from "../../../shared/utility";
import { AddNewStaffValidator } from "../../../shared/validators/AddNewStaff";
import { FormikErrorMessage } from "../../components";

export const AddNewStaff = () => {
  const params = useParams();
  const id = params.id;
  useEffect(() => {
    console.log("Clinic Id => ", id);
  }, []);

  const AddNewStaffFormik = useFormik<AddNewStaffValidator>({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      licence: "",
      password: "",
      staff: [],
    },
    onSubmit: (values) => {
      // API.Clinics.createNewStaffApi(values, id)
      API.Clinics.createNewStaffApi(id, values)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    validate: FormikValidator.validator(AddNewStaffValidator),
  });

  // const dispatch = useDispatch();
  // const clinic = useSelector((state) => state.clinic);
  // useEffect(() => {}, [dispatch]);

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Add New Staff</h2>
      </div>
      <div className="clinic-white-board">
        <h1>Clinic Details</h1>

        <div className="clinic-form-section">
          <form onSubmit={AddNewStaffFormik.handleSubmit}>
            <div className="label-input">
              <label htmlFor="">Clinic Name</label>
              <input
                type="text"
                name="firstName"
                id="firstName"
                placeholder="Clinic Name"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="firstName"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input">
              <label htmlFor="">Last Name</label>
              <input
                type="text"
                name="lastName"
                id="lastName"
                placeholder="Clinic Email"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="lastName"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input">
              <label htmlFor="">Clinic email</label>
              <input
                type="email"
                name="email"
                id="email"
                placeholder="Phone Number"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="email"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input">
              <label htmlFor="">phoneNumber</label>
              <input
                type="number"
                name="phoneNumber"
                id="phoneNumber"
                placeholder="phoneNumber"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="phoneNumber"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input ">
              <label htmlFor="">licence</label>
              <input
                type="text"
                name="licence"
                id="licence"
                placeholder="licence"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="licence"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            {/* 
            <div className="label-input">
              <label htmlFor="">Role</label>
              <input type="text" name="role" id="role" placeholder="Nurse" />
            </div> */}

            <div className="label-input">
              <label htmlFor="">password</label>
              <input
                type="password"
                name="password"
                id="licence-number"
                placeholder="password"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="password"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input">
              <label htmlFor="">role</label>
              <input
                type="text"
                name="staff"
                id="staff"
                placeholder="staff"
                onChange={AddNewStaffFormik.handleChange}
                onBlur={AddNewStaffFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="staff"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            {/* <div className="label-input">
              <label htmlFor="">Select Clinic</label>
              <select
                className="select-input clinic-select-organization"
                name="clinicId"
                onChange={AddNewStaffFormik.handleChange}
              >
                <option value={null} selected={true} disabled>
                  Select
                </option>
                {clinic.data.map((item) => (
                  <option value={item.id}>{item.name}</option>
                ))}
              </select>

              <FormikErrorMessage
                formik={AddNewStaffFormik}
                name="clinicId"
                render={(error) => <span className="error">{error}</span>}
              />
            </div> */}
            <div className="clinic-form-button">
              <button type="submit" className="solid-button">
                Add Add Staff
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
