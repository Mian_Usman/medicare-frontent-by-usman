import { useFormik } from "formik";
import React from "react";
import { API } from "../../../API/api";
import { FormikValidator } from "../../../shared/utility";
import { AddNewPatientValidator } from "../../../shared/validators/AddNewPatientValidator";
import { FormikErrorMessage } from "../../components";

export const AddNewPatient = () => {
  const AddNewPateintFormik = useFormik<AddNewPatientValidator>({
    initialValues: {
      name: "",
      email: "",
      address1: "",
      address2: "",
      phoneNumber: "",
      dob: "",
    },
    onSubmit: (values) => {
      API.Clinics.createNewPatient(values)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
      // console.log(values);
    },

    validate: FormikValidator.validator(AddNewPatientValidator),
  });

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Add New Pateint</h2>
      </div>
      <div className="clinic-white-board">
        <div className="patient-header-properties">
          <h1>Patient Details</h1>
          <div className="pateint-header-counter">
            Patient Counter <h3>00651</h3>
          </div>
        </div>
        <div className="clinic-form-section">
          <form onSubmit={AddNewPateintFormik.handleSubmit}>
            <div className="label-input">
              <label htmlFor="">Name</label>
              <input
                type="text"
                name="name"
                id="name"
                placeholder="Clinic Name"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="name"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="label-input">
              <label htmlFor="">Email</label>
              <input
                type="email"
                name="email"
                id="email"
                placeholder="Clinic Email"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="email"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="label-input">
              <label htmlFor="">Phone Number</label>
              <input
                type="number"
                name="phoneNumber"
                id="phoneNumber"
                placeholder="Phone Number"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="phoneNumber"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            {/* <div className="label-input">
              <label htmlFor="">Gender</label>
              <select
                name="gender"
                id="gender"
                onChange={AddNewPateintFormik.handleChange}
              >
                <option value="">male</option>
                <option value="">female</option>
                <option value="">not specified</option>
              </select>
            </div> */}
            <div className="label-input">
              <label htmlFor="">Address Line 1</label>
              <input
                type="date"
                name="dob"
                id="dob"
                placeholder="Clinic Address Line 1"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="dob"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="label-input">
              <label htmlFor="">Address Line 1</label>
              <input
                type="text"
                name="address1"
                id="address1"
                placeholder="Clinic Address Line 1"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="address1"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="label-input">
              <label htmlFor="">
                Address Line 2{" "}
                <span style={{ color: "#2cd889" }}>(opional)</span>
              </label>
              <input
                type="text"
                name="address2"
                placeholder="Clinic Address Line 2"
                onChange={AddNewPateintFormik.handleChange}
                onBlur={AddNewPateintFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={AddNewPateintFormik}
                name="address2"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>

            <div className="clinic-form-button">
              <button className="solid-button" type="submit">
                Add Patient
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
