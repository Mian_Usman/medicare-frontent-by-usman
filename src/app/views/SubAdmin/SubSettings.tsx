import React, { ChangeEvent, useState } from "react";
import { API } from "../../../API/api";
const img = require("../../asstes/gallery/settings-form-img.png");

export const SubSettings = () => {
  const [images, setimages] = useState<string[]>([]);
  const handleChangeFile = (event: ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const { files } = target;
    const file = files[0];
    API.Media.upload(file)
      .then((response) => {
        setimages((state) => [
          ...state,
          API.Media.getFileByName(response.data.filename),
        ]);
      })
      .catch(console.log);
  };

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Add New Pateint</h2>
      </div>
      <div className="clinic-white-board">
        <div className="form-sub-settings-image">
          <>
            {images.map((image, index) => {
              <div className="added-image" key={index}>
                <img src={image} className="file" alt="" />
              </div>;
            })}
          </>
          <img src={img} alt="" />
          <label htmlFor="file">
            <i className="fa fa-camera"></i>
            <input
              onChange={handleChangeFile}
              type="file"
              name="file"
              className="file"
              id="file"
              hidden
            />
          </label>
        </div>
        <div className="clinic-form-section">
          <form action="">
            <div className="label-input">
              <label htmlFor="">Name</label>
              <input
                type="text"
                name="clinic-name"
                id="clinic-name"
                placeholder="Clinic Name"
              />
            </div>

            <div className="label-input">
              <label htmlFor="">Password</label>
              <input
                type="password"
                name="password"
                id="password"
                placeholder="Password"
              />
            </div>

            <div className="label-input">
              <label htmlFor="">Address Line 1</label>
              <input
                type="text"
                name="address1"
                id="clinic-email"
                placeholder="Clinic Address Line 1"
              />
            </div>
            <div className="label-input">
              <label htmlFor="">
                Address Line 2{" "}
                <span style={{ color: "#2cd889" }}>(opional)</span>
              </label>
              <input
                type="email"
                name="address2"
                placeholder="Clinic Address Line 2"
              />
            </div>
            <div className="label-input">
              <label htmlFor="">Licence Number</label>
              <input
                type="number"
                name="licence-number-number"
                id="number"
                placeholder="Licence Number"
              />
            </div>
            <div className="label-input">
              <label htmlFor="">Licence Expiry Date</label>
              <input
                type="number"
                name="licence-number-expiry"
                id="number"
                placeholder="Licence Expiry Date"
              />
            </div>

            <div className="clinic-form-button">
              <button className="solid-button">Add Patient</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
