import { useState } from "react";
import { Button, Dropdown, FormControl, InputGroup } from "react-bootstrap";
import imag from "../../asstes/gallery/1threeDotsMenu.svg";
import { DeleteClinicModal } from "../../components/ClinicsComponets";
import { PateintTable } from "../../components/SubAdminComponents/PatientComponetns";
import { DeletePatientModal } from "../../components/SubAdminComponents/PatientComponetns/DeletePatientModal";
import { StaffTable } from "../../components/SubAdminComponents/StaffComponents";

export const Patient = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    setShow(false);
  };

  const clinicsDots = (
    <div className="dropdown-organization">
      <Dropdown className="header-dropdown-main">
        <Dropdown.Toggle className="header-dropdown transparents">
          <img src={imag} alt="" className="transparents" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="header-dropdown-menu">
          <Dropdown.Item className="header-dropdown-item">Edit</Dropdown.Item>
          <Dropdown.Item className="header-dropdown-item" onClick={handleShow}>
            Delete
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Pateints</h2>
      </div>
      <div className="clinic-white-board">
        <div className="seach-bar-select-menu">
          <div className="">
            <InputGroup className="">
              <FormControl
                className="Searchbaar-input"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="basic-addon2"
              />
              <Button className="searchbaar-button">
                <i className="fa fa-search"></i>
              </Button>
            </InputGroup>
          </div>

          <div className="staff-bar-select">
            <button className="Add-new-pateint-button">Add New Patient</button>
          </div>
        </div>
        <div className="clinic-table">
          <table>
            <thead>
              <tr style={{ fontWeight: "700" }}>
                <th scope="col">No.</th>
                <th scope="col">Patient Name</th>
                <th scope="col">Email </th>
                <th scope="col">Phone Number</th>
                <th scope="col">City</th>
              </tr>
            </thead>
            <tbody>
              <PateintTable
                Number="001"
                PatientName="Hammad"
                Email="Abdullah"
                PhoneNumber="Lahore"
                City="+92 304 8181784"
                clinicDots={clinicsDots}
              />
            </tbody>
          </table>
        </div>
      </div>
      {!!show && (
        <DeletePatientModal
          open={show}
          onClose={handleClose}
          onConfirm={handleConfirm}
        />
      )}
    </div>
  );
};
