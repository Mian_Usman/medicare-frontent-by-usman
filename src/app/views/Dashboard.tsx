import React from "react";
import { useEffect } from "react";
import { API } from "../../API/api";
import { useDispatch, useSelector } from "../../API/store";
import { useAuthContext } from "../../shared/contexts";
import { SubscriptionFee } from "../components";
const clinicIcon = require("../asstes/gallery/dashboard-clinic-icon.png");
const organizationIcon = require("../asstes/gallery/dashboard-organization-icon.png");
const subscriptionIcon = require("../asstes/gallery/dashboard-subscription-icon.png");

export const Dashboard = () => {
  const context = useAuthContext();

  const dispatch = useDispatch();
  const clinic = useSelector((state) => state.clinic);
  useEffect(() => {}, [dispatch]);

  return (
    <div className="dashboard-main-container">
      <div className="dashboard-container">
        <div className="top-three-cards">
          <div className="card1">
            <div className="img">
              <img src={clinicIcon} alt="" />
            </div>
            {/* {clinic.data.map((item) => ( */}
            <div className="cards-data">
              <p>Clinics</p>
              <div className="cards-h1">20</div>
            </div>
            {/* ))} */}
          </div>
          <div className="card1">
            <div className="img">
              <img src={organizationIcon} alt="" />
            </div>
            <div className="cards-data">
              <p>Organization</p>
              <div className="cards-h1">23</div>
            </div>
          </div>
          <div className="card1">
            <div className="img">
              <img src={subscriptionIcon} alt="" />
            </div>
            <div className="cards-data">
              <p>Subscription fee</p>
              <div className="cards-h1">$5900</div>
            </div>
          </div>
        </div>

        {/*  */}
        <div className="blocks-subscription-section">
          <div className="dashboard-empty-blocks">
            <div className="block-1"></div>
            <div className="block-1"></div>
          </div>

          <div className="subscription-fee-section">
            <div style={{ padding: 10 }} className="request-button">
              <h3 style={{ fontSize: "22px", fontWeight: "bold" }}>
                Pending Payments
              </h3>
              <li>Request All</li>
            </div>

            <SubscriptionFee
              name="CARE MD Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="ABCD Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="ETC Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="Madicare Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="Madicare Clinic"
              desgination="Admin Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="Madicare Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
            <div className="class-border-bottom">
              <div className="border"></div>
            </div>
            <SubscriptionFee
              name="Madicare Clinic"
              desgination="Admin: Hassan Don"
              requestsPayments="Request Payment"
            />
          </div>
        </div>
      </div>
    </div>
  );
};
