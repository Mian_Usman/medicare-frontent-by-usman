import React from "react";
import sample from "../../asstes/gallery/sample-img.jpg";
import { OragnizationProfileTabs } from "../../components/organizationComponents";

export const DetailsOranizationData = () => {
  return (
    <div className="organization-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Oraganizations</h2>
      </div>
      <div className="clinic-white-board">
        <div className="h4s">
          <h4>Profile</h4>
        </div>

        {/*  */}

        <div className="organization-profile-main-container">
          <div className="img-data">
            <div className="img-border">
              <div className="details-round-images">
                <img src={sample} alt="" />
              </div>
            </div>
            <div className="organiaztion-profile-info">
              <p
                style={{
                  padding: "5px 0",
                  fontWeight: "bold",
                  fontSize: "18px",
                }}
              >
                Hammad Organization
              </p>
              <p>who@gmail.com</p>
              <p>+92 333 12345678</p>
              <p>
                Office 40, 2nd Floor, Big city plaza, Liberty Roundabout, Lahore
              </p>
            </div>
          </div>

          <div className="organization-profile-edit-button">
            <button className="solid-button">Edit</button>
          </div>
        </div>

        {/*  */}

        <div className="organization-profile-members">
          <div className="h4s">
            <h4>Members</h4>
          </div>
          <OragnizationProfileTabs />
        </div>
        <div className="see-all">
          <p>See All</p>
        </div>
      </div>
    </div>
  );
};
