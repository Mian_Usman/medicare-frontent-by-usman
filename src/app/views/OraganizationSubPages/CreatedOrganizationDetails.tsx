import { OrganizationTabs } from "../../components/organizationComponents";

const img = require("../../asstes/gallery/settings-form-img.png");

export const CreateNewOrganization = () => {
  return (
    <div className="organization-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Oraganization</h2>
      </div>

      <div className="clinic-white-board">
        <div className="new-organzation-container">
          <div className="organization-form-section">
            <div className="form-organization-image">
              <img src={img} alt="" />
            </div>
            <form action="">
              <div className="label-input">
                <label htmlFor="">Name</label>
                <input type="name" name="name" id="-name" placeholder=" Name" />
              </div>
              <div className="label-input">
                <label htmlFor=""> Email</label>
                <input
                  type="email"
                  name="-email"
                  id="-email"
                  placeholder=" Email"
                />
              </div>
              <div className="label-input">
                <label htmlFor=""> Phone Number</label>
                <input
                  type="number"
                  name="number"
                  id="-number"
                  placeholder=" Phone Number"
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Organization Name</label>
                <input
                  type="organization"
                  name="organization"
                  id="organization"
                  placeholder="Organization Name"
                />
              </div>
            </form>
          </div>
          <div className="tabs-section-style">
            <OrganizationTabs />
          </div>
        </div>
        <div className="orgnization-add-save-changes-button ">
          <button className="solid-button">Save Changes</button>
        </div>
      </div>
      {/*  */}
    </div>
  );
};
