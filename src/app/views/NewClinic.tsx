import { useFormik } from "formik";
import { useEffect } from "react";
import { API } from "../../API/api";
import { Organization } from "../../API/api/Organzation";
import { useDispatch, useSelector } from "../../API/store";
import { OrganizationListThunk } from "../../API/thunk";
import { useAuthContext } from "../../shared/contexts";
import { FormikValidator } from "../../shared/utility";
import { AddNewClinicValidator } from "../../shared/validators";
import { FormikErrorMessage } from "../components";

export const NewClinic = () => {
  const context = useAuthContext();
  const dispatch = useDispatch();

  const AddNewClinicFormik = useFormik<AddNewClinicValidator>({
    initialValues: {
      name: "",
      email: "",
      address1: "",
      address2: "",
      phoneNumber: "",
      adminFirstName: "",
      adminLastName: "",
      plan: "",
      adminEmail: "",
      adminPh: "",
      password: "",
      organization: "",
    },
    onSubmit: (values) => {
      API.Clinics.create(values)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    validate: FormikValidator.validator(AddNewClinicValidator),
  });

  const organization = useSelector((state) => state.organization);
  useEffect(() => {
    dispatch(OrganizationListThunk());
  }, [dispatch]);

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Add New Clinic</h2>
      </div>
      <div className="clinic-white-board">
        <h1>Clinic Details</h1>

        <div className="clinic-form-section">
          <form onSubmit={AddNewClinicFormik.handleSubmit}>
            <>
              <div className="label-input">
                <label htmlFor="">Clinic Name</label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  placeholder="Clinic Name"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="name"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Clinic Email</label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Clinic Email"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="email"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Phone Number</label>
                <input
                  type="number"
                  name="phoneNumber"
                  id="phoneNumber"
                  placeholder="Phone Number"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="phoneNumber"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input choose-plan-input-div">
                <label htmlFor="">Subscription Fee Monthly</label>
                <div className="choose-plan-input">
                  <input
                    type="text"
                    name="plan"
                    id="plan"
                    placeholder="Subscription Fee Monthly"
                    onChange={AddNewClinicFormik.handleChange}
                    onBlur={AddNewClinicFormik.handleBlur}
                  />
                  <button className="light-button">Choose plan</button>
                  <FormikErrorMessage
                    formik={AddNewClinicFormik}
                    name="plan"
                    render={(error) => <span className="error">{error}</span>}
                  />
                </div>
              </div>
              <div className="label-input">
                <label htmlFor="">Clinic Address Line 1</label>
                <input
                  type="text"
                  name="address1"
                  id="address1"
                  placeholder="Clinic Address Line 1"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="address1"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">
                  Clinic Address Line 2
                  <span style={{ color: "#2cd889" }}>(opional)</span>
                </label>
                <input
                  type="text"
                  name="address2"
                  id="address2"
                  placeholder="Clinic Address Line 2"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="address2"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div style={{ width: "100%", paddingTop: "20px" }}>
                <h1>Admin Clinic Details</h1>
              </div>
              <div className="label-input">
                <label htmlFor="">Admin First Name</label>
                <input
                  type="name"
                  name="adminFirstName"
                  id="adminFirstName"
                  placeholder="Admin First Name"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="adminFirstName"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Admin Last Name</label>
                <input
                  type="text"
                  name="adminLastName"
                  id="adminLastName"
                  placeholder="Admin Email"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="adminLastName"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Admin Email</label>
                <input
                  type="email"
                  name="adminEmail"
                  id="adminEmail"
                  placeholder="Admin Phone Number"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="adminEmail"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Admin Phone</label>
                <input
                  type="number"
                  name="adminPh"
                  id="adminPh"
                  placeholder="adminPh"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="adminPh"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input">
                <label htmlFor="">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password"
                  onChange={AddNewClinicFormik.handleChange}
                  onBlur={AddNewClinicFormik.handleBlur}
                />
                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="password"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="label-input ">
                <label htmlFor="">Organization</label>

                <select
                  className="select-input clinic-select-organization"
                  name="organization"
                  onChange={AddNewClinicFormik.handleChange}
                >
                  <option value={null} selected={true} disabled>
                    Select
                  </option>
                  {organization.data.map((item) => (
                    <option value={item.id}>{item.name}</option>
                  ))}
                </select>

                <FormikErrorMessage
                  formik={AddNewClinicFormik}
                  name="organization"
                  render={(error) => <span className="error">{error}</span>}
                />
              </div>
              <div className="clinic-form-button">
                <button className="solid-button" type="submit">
                  Add Clinic
                </button>
              </div>
            </>
          </form>
        </div>
      </div>
    </div>
  );
};
