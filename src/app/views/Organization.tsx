import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import Dropsdown from "../asstes/gallery/1threeDotsMenu.svg";
import { NavLink } from "react-router-dom";
import {
  AddDoctorsModal,
  AddMaganagingStaffModal,
  DeleteOrganizationModal,
} from "../components/organizationComponents";
import { CreateNewOrganizationModal } from "../components/organizationComponents/CreateNewOrganzationModal";
import { useDispatch, useSelector } from "../../API/store";
import { OrganizationListThunk } from "../../API/thunk/Organization-thunk";

export const Organization = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    setShow(false);
  };

  const [Doctorshow, setDoctorShow] = useState(false);

  const DochandleClose = () => {
    setDoctorShow(false);
  };
  const DochandleShow = () => {
    setDoctorShow(true);
  };
  const DochandleConfirm = () => {
    setDoctorShow(false);
  };

  const [CreateOrganizationModal, setShowCreateOrganizationModal] =
    useState(false);

  const CloseOrganizationModal = () => {
    setShowCreateOrganizationModal(false);
  };
  const ShowOrganizationModal = () => {
    setShowCreateOrganizationModal(true);
  };
  const hideOrganizationModalConfirm = () => {
    setShowCreateOrganizationModal(false);
  };

  // Delete Modal
  const [showDelete, setShowDelete] = useState(null);

  const handleShowDelete = (id: string) => () => {
    setShowDelete(id);
  };
  const handleCloseDelete = () => {
    setShowDelete(null);
  };

  const dispatch = useDispatch();
  const organization = useSelector((state) => state.organization);
  useEffect(() => {
    dispatch(OrganizationListThunk());
  }, [dispatch]);

  const clinicsDots = (
    <div className="dropdown-organization">
      <Dropdown className="header-dropdown-main" align="end">
        <Dropdown.Toggle className="header-dropdown transparents">
          <img src={Dropsdown} alt="" className="transparents" />
        </Dropdown.Toggle>
        {organization.data.map((item) => (
          <Dropdown.Menu className="header-dropdown-menu" key={item.id}>
            <Dropdown.Item
              className="header-dropdown-item"
              onClick={handleShow}
            >
              Add Staff
            </Dropdown.Item>
            <Dropdown.Item className="header-dropdown-item">
              <NavLink style={{ color: "black" }} to={"/new-clinic"}>
                Add Clinic
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item
              className="header-dropdown-item"
              onClick={DochandleShow}
            >
              Add Doctor
            </Dropdown.Item>
            <Dropdown.Item
              className="header-dropdown-item"
              onClick={DochandleShow}
            >
              Edit
            </Dropdown.Item>

            <Dropdown.Item
              className="header-dropdown-item"
              onClick={handleShowDelete(item.id)}
              style={{ color: "red" }}
            >
              Delete
            </Dropdown.Item>
          </Dropdown.Menu>
        ))}
      </Dropdown>
    </div>
  );

  return (
    <>
      <div className="organization-main-container">
        <div className="clinic-container">
          <h2 style={{ padding: "15px" }}>Oraganizations</h2>
        </div>
        <div className="clinic-white-board">
          <div className="seach-bar-select-menu">
            <div className=""></div>

            <div className="create-bar-organization">
              {/* <NavLink to={"/create-new-organization"}> */}
              {/* </NavLink> */}
              <button className="solid-button" onClick={ShowOrganizationModal}>
                Create New Organization
              </button>
            </div>
          </div>
          <div className="pagination-flex-end">
            <div className="clinic-table">
              <table>
                <thead>
                  <tr style={{ fontWeight: "700" }}>
                    <th scope="col">Name</th>
                    <th scope="col">Description </th>
                    <th scope="col">Doctor</th>
                    <th scope="col">Clinics</th>
                    <th scope="col">Others</th>
                  </tr>
                </thead>

                <tbody>
                  {organization.data.map((org) => (
                    <tr key={org.id}>
                      <td data-label="Organization">
                        <NavLink
                          to={"/details-organization-data"}
                          style={{ color: "#a6b1c2" }}
                        >
                          {org.name}
                        </NavLink>
                      </td>

                      <td>{org.description}</td>
                      <td>{org.doctors}</td>
                      <td>{org.clinics}</td>
                      <td>{org.others}</td>
                      <td>{clinicsDots}</td>
                    </tr>
                  ))}
                </tbody>
              </table>

              {/* <div>
                <video width="620" height="240" controls>
                  <source src="rtsp://admin:LYRAQB@192.168.0.4:554/H.264" />
                </video>
              </div> */}
            </div>

            {/* <div className="pagination-container">
              <div></div>
              <div className="pagination">
                <a href="#">&laquo;</a>
                <a href="#" className="active">
                  1
                </a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">6</a>
                <a href="#">&raquo;</a>
              </div>
            </div> */}
          </div>
        </div>
      </div>
      {!!show && (
        <AddMaganagingStaffModal
          open={show}
          onClose={handleClose}
          onConfirm={handleConfirm}
        />
      )}
      {!!Doctorshow && (
        <AddDoctorsModal
          open={Doctorshow}
          onClose={DochandleClose}
          onConfirm={DochandleConfirm}
        />
      )}
      {!!CreateOrganizationModal && (
        <CreateNewOrganizationModal
          open={CreateOrganizationModal}
          onClose={CloseOrganizationModal}
          onConfirm={hideOrganizationModalConfirm}
        />
      )}

      <DeleteOrganizationModal
        id={showDelete}
        open={!!showDelete}
        onClose={handleCloseDelete}
      />
    </>
  );
};
