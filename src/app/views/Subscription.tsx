import React, { useState } from "react";
import { Button, Dropdown, FormControl, InputGroup } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { SubscriptionTableList } from "../components";
import imag from "../asstes/gallery/1threeDotsMenu.svg";

export const Subscription = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    setShow(false);
  };

  const [Doctorshow, setDoctorShow] = useState(false);

  const DochandleClose = () => {
    setDoctorShow(false);
  };
  const DochandleShow = () => {
    setDoctorShow(true);
  };
  const DochandleConfirm = () => {
    setDoctorShow(false);
  };

  const clinicsDots = (
    <div className="dropdown-organization">
      <Dropdown className="header-dropdown-main" align="end">
        <Dropdown.Toggle className="header-dropdown transparents">
          <img src={imag} alt="" className="transparents" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="header-dropdown-menu">
          <Dropdown.Item className="header-dropdown-item" onClick={handleShow}>
            Add Managing Staff
          </Dropdown.Item>
          <Dropdown.Item className="header-dropdown-item">
            <NavLink style={{ color: "black" }} to={"/new-clinic"}>
              Add Clinic
            </NavLink>
          </Dropdown.Item>
          <Dropdown.Item
            className="header-dropdown-item"
            onClick={DochandleShow}
          >
            Add Doctor
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );

  const recievedbtn = <button className="recieved-button">Recieved</button>;
  const pendingbtn = <button className="pending-button">Pending</button>;

  return (
    <div className="clinic-main-container">
      <div className="clinic-container">
        <h2 style={{ padding: "15px" }}>Subscriptions</h2>
      </div>
      <div className="clinic-white-board">
        <div className="seach-bar-select-menu">
          <div className="">
            <InputGroup className="">
              <FormControl
                className="Searchbaar-input"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="basic-addon2"
              />
              <Button className="searchbaar-button">
                <i className="fa fa-search"></i>
              </Button>
            </InputGroup>
          </div>

          <div className="seach-bar-select">
            <select name="All" id="select">
              <option value="all">All</option>
              <option value="pending">Pending</option>
              <option value="recieved">Recieved</option>
            </select>
          </div>
        </div>
        <div className="clinic-table">
          <table>
            <thead>
              <tr style={{ fontWeight: "700" }}>
                <th scope="col">No.</th>
                <th scope="col">Clinic Name</th>
                <th scope="col">Clinic Admin</th>
                <th scope="col">Clinic Email</th>
                <th scope="col">Clinic Phone Number</th>
                <th scope="col">Subscription</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-label="Number">Usman</td>
                <td data-label="Clinic Details">Hospital abc</td>
                <td data-label="Clinic Admin">Main admin</td>
                <td data-label="Clinic Email">usman@gmail.com</td>
                <td data-label="Clinic Phone Number">03048181784</td>
                <td data-label="buttons">
                  <button className="pending-button">Pending</button>
                </td>
                <td data-label="Clinic Dots">{clinicsDots}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
