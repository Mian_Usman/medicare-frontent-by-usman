import React from "react";
import { Modal } from "react-bootstrap";

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

export const ProfileDetailsOnChat = (props: Props) => {
  return (
    <Modal
      show={props.open}
      onHide={props.onClose}
      className="chat-pofile-details-container"
    ></Modal>
  );
};
