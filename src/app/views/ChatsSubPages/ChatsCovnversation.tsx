import dots from "../../asstes/gallery/three-dots.svg";
import clip from "../../asstes/gallery/clip.svg";
import image from "../../asstes/gallery/icons-video-img.svg";
import send from "../../asstes/gallery/send-button.svg";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import { ConversationProfileViews } from "../../components/ConversationProfileViews";
import {
  LeftMessgaeSection,
  RightMessgaeSection,
} from "../../components/MessgaeSection";
import profile1 from "../../asstes/gallery/profile1.png";
import img2 from "../../asstes/gallery/settings-form-img.png";
import sample from "../../asstes/gallery/sample-img.jpg";
import { ConversationPlusButton } from "../../components/chatsComponents";
import { NavLink } from "react-router-dom";
import { ProfileDetailsModal } from "../../components/chatsComponents/ProfileDetailsModal";

export const ChatsConversation = () => {
  function dropdownss() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  // Close the dropdown menu if the user clicks outside of it
  window.onclick = function (event) {
    if (!event.target.matches(".dropbtn")) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains("show")) {
          openDropdown.classList.remove("show");
        }
      }
    }
  };

  const MenuDots = (
    <div className="dropdown dropdown-menu-lg-start">
      <div>
        <img
          onClick={dropdownss}
          className="svg-iconss dropbtn"
          src={dots}
          alt=""
        />
      </div>
      <div id="myDropdown" className="dropdown-content">
        <a href="#">delete</a>
      </div>
    </div>
  );

  return (
    <div className="chats-main-container">
      <div className="chats-container">
        <h2 style={{ padding: "15px" }}>Chats</h2>
      </div>
      <div className="white-board-devision">
        <div className="chats-white-board-1">
          <div className="conversation-section">
            <div className="conversation-searchbar">
              <InputGroup>
                <FormControl
                  className="Searchbaar-input"
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                />
                <Button className="searchbaar-button">
                  <i className="fa fa-search"></i>
                </Button>
              </InputGroup>
            </div>
            <div className="scrollbar">
              <div className="conversation-groups-plus">
                <h4>Groups</h4>

                <ConversationPlusButton />
              </div>

              {/* Chats-groups */}
              <div className="group-sections">
                <ConversationProfileViews
                  profile={img2}
                  name="Hammad"
                  para="hi bro whats up?"
                  date="today"
                  newMsg="5"
                />
                <ConversationProfileViews
                  profile={sample}
                  name="Office"
                  para="hi whats going on bro ...?"
                  date="today"
                  newMsg="2"
                />
              </div>
              <div
                className="conversation-groups-plus"
                style={{ marginTop: "20px" }}
              >
                <h4>Chats</h4>
                {/* <ConversationPlusButton />*/}
              </div>
              <div className="group-sections">
                <ConversationProfileViews
                  profile={img2}
                  name="Hammad"
                  para="hi bro whats up?"
                  date="today"
                  newMsg="5"
                />
              </div>
            </div>
          </div>
        </div>
        {/*  */}
        {/*  */}
        <div className="chats-white-board-2">
          <div className="chatting-container">
            <div className="profile-data">
              <div className="img-name-type">
                <ProfileDetailsModal />
                <i className="fa fa-cicle online-dots"></i>

                <div className="name-typing-flex">
                  <h4>Hammad</h4>
                  <div>typing.....</div>
                </div>
              </div>
              <div className="properties">
                <NavLink to={"/video-calling"}>
                  <img className="svg-icon" src={image} alt="123" />
                </NavLink>

                <div>{MenuDots}</div>
                {/* <img className="svg-iconss" src={dots} alt={} /> */}
              </div>
            </div>
            {/*  */}
            <div className="scrollbar">
              <div
                style={{
                  padding: 20,
                  height: "auto",
                  display: "flex",
                  flexDirection: "column",
                  gap: 10,
                  width: "100%",
                }}
              >
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="whatsmade that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan before  ? up bro?"
                  time="12:22PM"
                />
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="lets make a plan to go to northern areas ok Pakistan"
                  time="12:22PM"
                />
                <RightMessgaeSection
                  profile={sample}
                  message="i am fine bro whats about u?"
                  time="12:25PM"
                />
                <RightMessgaeSection
                  profile={sample}
                  message="okay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan beforeokay sure i made that plan before  ?"
                  time="12:25PM"
                />
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="lets make a plan to go to northern areas ok Pakistan"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="letorthern areas ok Pakistan"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="lets make"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="lets maern areas ok Pakistan"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="luck"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="like"
                  time="12:22PM"
                />{" "}
                <LeftMessgaeSection
                  profile={img2}
                  name="Hammad"
                  message="lets make a plan to go to northern areas ok Pakistan"
                  time="12:22PM"
                />
              </div>
            </div>
            {/*  */}
            <div className="input-data">
              <div className="input-fields-with-logo">
                <div className="flex">
                  <img className="border-left" src={clip} alt="" />
                  <input type="text" placeholder="Enter your message" />
                </div>
                <button className="input-button-style">
                  <img className="border-" src={send} alt="" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
