import { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { useDispatch, useSelector } from "../../API/store";
import { ClinicsListThunk } from "../../API/thunk";
import { AddNewClinicInterface } from "../../shared/interfaces";
import Dropsdown from "../asstes/gallery/1threeDotsMenu.svg";
import {
  DeleteClinicModal,
  EditClinicModal,
} from "../components/ClinicsComponets";

export const Clinics = () => {
  const [showEditModal, setShowEditClinicModal] =
    useState<AddNewClinicInterface>(null);

  //Edit

  const handleClinicShow = (clinicInterface: AddNewClinicInterface) => () => {
    setShowEditClinicModal(clinicInterface);
  };

  const handleClinicClose = () => {
    setShowEditClinicModal(null);
  };

  //Delete

  const [show, setShow] = useState(null);

  const handleShow = (id: string) => () => {
    setShow(id);
  };
  const handleClose = () => {
    setShow(null);
  };

  //

  const dispatch = useDispatch();
  const clinic = useSelector((state) => state.clinic);
  useEffect(() => {
    dispatch(ClinicsListThunk());
  }, [dispatch]);

  const clinicsDots = (
    <div className="dropdown-organization">
      <Dropdown className="header-dropdown-main">
        <Dropdown.Toggle className="header-dropdown transparents">
          <img src={Dropsdown} alt="" className="transparents" />
        </Dropdown.Toggle>
        {clinic.data.map((item) => (
          <Dropdown.Menu className="header-dropdown-menu" key={item.id}>
            <Dropdown.Item
              className="header-dropdown-item"
              onClick={handleClinicShow(item)}
            >
              Edit
            </Dropdown.Item>

            <Dropdown.Item
              className="header-dropdown-item"
              onClick={handleShow(item.id)}
            >
              Delete
            </Dropdown.Item>
          </Dropdown.Menu>
        ))}
      </Dropdown>
    </div>
  );

  return (
    <>
      <div className="clinic-main-container">
        <div className="clinic-container">
          <h2 style={{ padding: "15px" }}>Clinic</h2>
        </div>
        <div className="clinic-white-board">
          <div className="clinic-table">
            <table>
              <thead>
                <tr style={{ fontWeight: "700" }}>
                  <th scope="col">No.</th>
                  <th scope="col">Clinic Name</th>
                  <th scope="col">Clinic Admin</th>
                  <th scope="col">Address</th>
                  <th scope="col">Clinic Email</th>
                  <th scope="col">Clinic Phone Number</th>
                </tr>
              </thead>
              <tbody>
                {clinic.data.map((cli) => (
                  <tr key={cli.id}>
                    <td>{cli.serialNumber}</td>
                    <td>{cli.name}</td>
                    <td>{cli.verifiedPublicHealth}</td>
                    <td>{cli.address1}</td>
                    <td>{cli.email}</td>
                    <td>{cli.phoneNumber}</td>
                    <td>{clinicsDots}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      {!!show && (
        <DeleteClinicModal open={!!show} id={show} onClose={handleClose} />
      )}

      {!!showEditModal && (
        <EditClinicModal
          openEditor={!!showEditModal}
          clinic={showEditModal}
          closeEditor={handleClinicClose}
        />
      )}
    </>
  );
};
