import dots from "../asstes/gallery/three-dots.svg";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import { ConversationProfileViews } from "../components/ConversationProfileViews";
import img2 from "../asstes/gallery/settings-form-img.png";
import sample from "../asstes/gallery/sample-img.jpg";
import emptyChatPage from "../asstes/gallery/empty-chat-page.svg";
import { NavLink } from "react-router-dom";
import { ConversationPlusButton } from "../components/chatsComponents";

export const Chat = () => {
  function dropdownss() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  // Close the dropdown menu if the user clicks outside of it

  window.onclick = function (event) {
    if (!event.target.matches(".dropbtn")) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains("show")) {
          openDropdown.classList.remove("show");
        }
      }
    }
  };

  return (
    <div className="chats-main-container">
      <div className="chats-container">
        <h2 style={{ padding: "15px" }}>Chats</h2>
      </div>
      <div className="white-board-devision">
        <div className="chats-white-board-1">
          <div className="conversation-section">
            <div className="conversation-searchbar">
              <InputGroup>
                <FormControl
                  className="Searchbaar-input"
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                />
                <Button className="searchbaar-button">
                  <i className="fa fa-search"></i>
                </Button>
              </InputGroup>
            </div>
            <div className="scrollbar">
              <div className="conversation-groups-plus">
                <h4>Groups</h4>
                <ConversationPlusButton />
              </div>

              {/* Chats-groups */}
              <div className="group-sections">
                <ConversationProfileViews
                  profile={img2}
                  name="Hammad"
                  para="hi bro whats up?"
                  date="today"
                  newMsg="5"
                />
                <ConversationProfileViews
                  profile={sample}
                  name="Office"
                  para="hi whats going on bro ...?"
                  date="today"
                  newMsg="2"
                />
              </div>
              <div
                className="conversation-groups-plus"
                style={{ marginTop: "20px" }}
              >
                <h4>Chats</h4>
              </div>
              <div className="group-sections">
                <NavLink to={"/chats-conversation"}>
                  <ConversationProfileViews
                    profile={img2}
                    name="Hammad"
                    para="hi bro whats up?"
                    date="today"
                    newMsg="5"
                  />
                </NavLink>
              </div>
            </div>
          </div>
        </div>
        {/*  */}
        {/*  */}
        <div className="chats-white-board-2">
          <div className="chats-empty-page-layout">
            <img src={emptyChatPage} alt="" />
            <p>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolor
              autem aliquid dolore consectetur iste. Aut quasi blanditiis porro
              necessitatibus adipisci, vitae nihil ipsum nostrum sequi
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
