import React from "react";
const profile1 = require("../asstes/gallery/profile1.png");

interface senderProps {
  profile: string;
  name: string;
  message: string;
  time: string;
}
interface recieverProps {
  profile: string;
  message: string;
  time: string;
}

export const LeftMessgaeSection = (props: senderProps) => {
  return (
    <div className="left-messages-section">
      <div className="message-container">
        <img src={props.profile} alt="" />
        <div className="message-container">
          <div className="message-flex">
            <h4>{props.name}</h4>
            <p>{props.message}</p>
            <div className="time">{props.time}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const RightMessgaeSection = (props: recieverProps) => {
  return (
    <div className="right-messages-section">
      <div className="message-container">
        <div className="message-container">
          <div className="message-flex">
            <p>{props.message}</p>
            <div className="time">{props.time}</div>
          </div>
          <img src={props.profile} alt="" />
        </div>
      </div>
    </div>
  );
};
