import React from "react";

interface staff {
  Number: string;
  DoctorName: string;
  speciality: string;
  Email: string;
  PhoneNumber: string;
  LicenceExpiry: string;
  clinicDots: any;
}

export const StaffTable = (propis: staff) => {
  return (
    <tr>
      <td data-label="Number">{propis.Number}</td>
      <td data-label="Doctor Name">{propis.DoctorName}</td>
      <td data-label="speciality">{propis.speciality}</td>
      <td data-label="Email">{propis.Email}</td>
      <td data-label="phone">{propis.PhoneNumber}</td>
      <td data-label="licence">{propis.LicenceExpiry}</td>
      <td data-label="Clinic Dots" style={{ cursor: "pointer" }}>
        {propis.clinicDots}
      </td>
    </tr>
  );
};
