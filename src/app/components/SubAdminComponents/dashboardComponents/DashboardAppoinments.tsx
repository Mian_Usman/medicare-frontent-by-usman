import React from "react";
const profileImg = require("../../../asstes/gallery/sub-profile-img.png");

interface Props {
  name: string;
  desgination: string;
  requestsPayments: string;
}

export const DashboardAppoinments = (props: Props) => {
  return (
    <div className="appointments-list">
      <div className="profile-info">
        <img src={profileImg} alt="" />
        <div className="name-designation">
          <div className="h3">{props.name}</div>
          <p>{props.desgination}</p>
        </div>
      </div>
      <div className="appointment-name-designation">
        <p style={{ color: "#00E9E9" }}>General</p>
        <p>12:30PM to 01:30PM</p>
      </div>
    </div>
  );
};
