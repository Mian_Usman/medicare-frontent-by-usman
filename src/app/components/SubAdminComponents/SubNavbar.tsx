import { Link, NavLink } from "react-router-dom";
const logo = require("../../asstes/gallery/logo.png");
const dashboard = require("../../asstes/gallery/dashboard.png");
const clinic = require("../../asstes/gallery/clinic.png");
const subscription = require("../../asstes/gallery/subscription.png");
const organization = require("../../asstes/gallery/organization.png");
const chat = require("../../asstes/gallery/chat.png");
const settings = require("../../asstes/gallery/settings.png");

export const SubNavbar = () => {
  return (
    <div className="navbar-root-container">
      <div className="main-navbar-container">
        <div className="navbar-logo">
          <img src={logo} alt="Web Logo" />
        </div>
        <div className="add-clinc">
          <Link to={"/add-new-patient"}>
            <div className="add-new-clinic">
              <i className="fa fa-plus-circle"></i>
              Add New Pateint
            </div>
          </Link>
          <Link to={"/add-new-staff"}>
            <div className="add-new-staff">
              <i className="fa fa-plus-circle"></i>
              Add New Staff
            </div>
          </Link>
        </div>

        <div className="navbar-menu-options">
          <div className="padding">
            <NavLink to={"/sub-dashboard"}>
              <div className="navbar-menus">
                <img src={dashboard} alt="" />
                Dashboard
              </div>
            </NavLink>

            <NavLink to={"/staff"}>
              <div className="navbar-menus">
                <img src={clinic} alt="" />
                Staff
              </div>
            </NavLink>

            <NavLink to={"/patient"}>
              <div className="navbar-menus">
                <img src={subscription} alt="" />
                Patient
              </div>
            </NavLink>

            <NavLink to={"/organization"}>
              <div className="navbar-menus">
                <img src={organization} alt="" />
                Appointments
              </div>
            </NavLink>

            <NavLink to={"/chat"}>
              <div className="navbar-menus">
                <img src={chat} alt="" />
                Chat
              </div>
            </NavLink>

            <NavLink to={"/sub-setting"}>
              <div className="navbar-menus">
                <img src={settings} alt="" />
                Setting
              </div>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
};
