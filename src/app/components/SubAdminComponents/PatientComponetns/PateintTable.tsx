import React from "react";

interface Patient {
  Number: string;
  PatientName: string;
  Email: string;
  PhoneNumber: string;
  City: string;
  clinicDots: any;
}

export const PateintTable = (propis: Patient) => {
  return (
    <tr>
      <td data-label="Number">{propis.Number}</td>
      <td data-label="Patient Name">{propis.PatientName}</td>
      <td data-label="speciality">{propis.Email}</td>
      <td data-label="phone">{propis.PhoneNumber}</td>
      <td data-label="licence">{propis.City}</td>
      <td data-label="Clinic Dots" style={{ cursor: "pointer" }}>
        {propis.clinicDots}
      </td>
    </tr>
  );
};
