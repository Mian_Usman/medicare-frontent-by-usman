import React from "react";
import { Modal } from "react-bootstrap";

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
}
export const DeletePatientModal = (props: Props) => {
  return (
    <Modal
      show={props.open}
      onHide={props.onClose}
      className="modal-main-container"
    >
      <div className="delete-button-modal-main-container">
        <div className="properties">
          <p>Are you sure you want to delete this Patient?</p>
          <div className="buttons">
            <button className="empty-light-button" onClick={props.onClose}>
              Cancel
            </button>
            <button className="solid-button">Delete</button>
          </div>
        </div>
      </div>
    </Modal>
  );
};
