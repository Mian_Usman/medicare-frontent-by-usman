import dots from "../../asstes/gallery/three-dots.svg";
import VideoCalling from "../../asstes/gallery/videoCallingProfileImage.svg";
import videoCallingVideoButton from "../../asstes/gallery/1videoCallingCameraImage.svg";
import videoCallingVoiceButton from "../../asstes/gallery/1VideoCallingVoiceButton.svg";
import videoCallingAddPersonButton from "../../asstes/gallery/1videoCallingAddPersonButton.svg";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import { ConversationProfileViews } from "../../components/ConversationProfileViews";
import profile1 from "../../asstes/gallery/profile1.png";
import img2 from "../../asstes/gallery/settings-form-img.png";
import sample from "../../asstes/gallery/sample-img.jpg";
import { ConversationPlusButton } from "../../components/chatsComponents";

export const VideoCallings = () => {
  function dropdownss() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  window.onclick = function (event) {
    if (!event.target.matches(".dropbtn")) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains("show")) {
          openDropdown.classList.remove("show");
        }
      }
    }
  };

  const MenuDots = (
    <div className="dropdown dropdown-menu-lg-start">
      <div>
        <img
          onClick={dropdownss}
          className="svg-iconss dropbtn"
          src={dots}
          alt=""
        />
      </div>
      <div id="myDropdown" className="dropdown-content">
        <a href="#">delete</a>
      </div>
    </div>
  );

  return (
    <div className="chats-main-container">
      <div className="chats-container">
        <h2 style={{ padding: "15px" }}>Chats</h2>
      </div>
      <div className="white-board-devision">
        <div className="chats-white-board-1">
          <div className="conversation-section">
            <div className="conversation-searchbar">
              <InputGroup>
                <FormControl
                  className="Searchbaar-input"
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                />
                <Button className="searchbaar-button">
                  <i className="fa fa-search"></i>
                </Button>
              </InputGroup>
            </div>
            <div className="scrollbar">
              <div className="conversation-groups-plus">
                <h4>Groups</h4>
                <ConversationPlusButton />
              </div>

              {/* Chats-groups */}
              <div className="group-sections">
                <ConversationProfileViews
                  profile={img2}
                  name="Hammad"
                  para="hi bro whats up?"
                  date="today"
                  newMsg="5"
                />
                <ConversationProfileViews
                  profile={sample}
                  name="Office"
                  para="hi whats going on bro ...?"
                  date="today"
                  newMsg="2"
                />
              </div>
              <div
                className="conversation-groups-plus"
                style={{ marginTop: "20px" }}
              >
                <h4>Chats</h4>
              </div>
              <div className="group-sections">
                <ConversationProfileViews
                  profile={img2}
                  name="Hammad"
                  para="hi bro whats up?"
                  date="today"
                  newMsg="5"
                />
              </div>
            </div>
          </div>
        </div>
        {/*  */}
        {/*  */}
        <div className="chats-white-board-2">
          <div className="chatting-container">
            <div className="profile-data">
              <div className="img-name-type">
                <img src={profile1} alt="" />
                <i className="fa fa-cicle online-dots"></i>

                <div className="name-typing-flex">
                  <h4 style={{ color: "#2CD889" }}>Hammad</h4>
                  <div>
                    Lorem ipsum dolor sit, amet consectetur adipisicing.
                  </div>
                </div>
              </div>
              <div className="properties">
                <div>{MenuDots}</div>
              </div>
            </div>
            {/*  */}

            <div className="video-calling-main">
              <div className="profile-image">
                <img src={VideoCalling} alt="" />
              </div>
              <p>Calling...</p>
            </div>

            {/*  */}
            <div className="input-data">
              <div className="video-callig-input-fields-with-logo">
                <div className="flex">
                  <p>00:23:45</p>
                  <div className="icons">
                    <div className="icons-border">
                      <img src={videoCallingVideoButton} alt="" />
                    </div>
                    <div className="icons-border">
                      <img
                        style={{ width: "15px" }}
                        src={videoCallingVoiceButton}
                        alt=""
                      />
                    </div>
                    <div className="icons-border">
                      <img src={videoCallingAddPersonButton} alt="" />
                    </div>
                  </div>
                  <button className="end-call">End Call</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
