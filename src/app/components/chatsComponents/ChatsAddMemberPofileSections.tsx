import React from "react";
import img from "../../asstes/gallery/profile1.png";

export const ChatsAddMemberPofileSections = () => {
  return (
    <div>
      <div className="modal-groups-members">
        <div className="modal-profile-container">
          <div className="img-name-flex">
            <div className="conversation-round-images">
              <img src={img} alt="" />
              <i className="fa fa-cicle online-dot"></i>
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <div>
                <div className="name-para">
                  <h4>Hassan </h4>
                  <p>Lorem ipsum dolor sit.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="checkbox-form-group">
          <input
            type="checkbox"
            name="checkbox"
            id="checkbox"
            className="checkbox"
          />
        </div>
      </div>
    </div>
  );
};
