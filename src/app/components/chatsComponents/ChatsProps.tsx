import Modal from "react-bootstrap/Modal";
import { ChatModalAddGroupMember } from "./ChatModalAddGroupMember";

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

export const ChatsProps = (props: Props) => {
  return (
    <Modal
      show={props.open}
      onHide={props.onClose}
      className="modal-main-container"
    >
      <div className="modals-container">
        <div className="modals-inner">
          <h3>Create Group</h3>
        </div>
        <div className="modals-inputs">
          <div className="inputs">
            <label htmlFor="">Group Name</label>
            <input
              type="text"
              name="group-name"
              id="group-name"
              placeholder="Group Name"
            />
          </div>
          <div className="inputs">
            <label htmlFor="">Description </label>
            <input
              type="text"
              name="description-"
              id="description-"
              placeholder="description "
            />
          </div>
          <div className="inputs">
            <label htmlFor="">Group Member</label>
            <input
              type="text"
              name="group-member"
              id="group-member"
              placeholder="Group Member"
            />
          </div>
        </div>

        <div>
          <ChatModalAddGroupMember />
        </div>

        <div
          className="modals-buttons-width"
          style={{
            display: "flex",
            justifyContent: "flex-end",
            width: "450px",
            marginTop: "10px",
            padding: "10px 0",
          }}
        >
          <button
            onClick={props.onClose}
            className="solid-button"
            style={{ padding: "5px 20px" }}
          >
            Create Group
          </button>
        </div>
      </div>
    </Modal>
  );
};
