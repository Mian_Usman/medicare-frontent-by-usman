import React, { useState } from "react";
import { ProfileDetailsOnChat } from "../../views/ChatsSubPages/ProfileDetailsOnChat";
import profile1 from "../../asstes/gallery/profile1.png";

export const ProfileDetailsModal = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    setShow(false);
  };
  return (
    <div onClick={handleShow}>
      <img src={profile1} alt="" />
      {show && (
        <ProfileDetailsOnChat
          open={show}
          onClose={handleClose}
          onConfirm={handleConfirm}
        />
      )}
    </div>
  );
};
