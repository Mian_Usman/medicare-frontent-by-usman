import React, { useState } from "react";
import { ChatsProps } from "./ChatsProps";

export const ChatsModals = (): JSX.Element => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    console.log("It Confirmed!");
    setShow(false);
  };

  return (
    <div className="reds">
      <h1>Click on the Button to visit our Help Department</h1>
      {show && (
        <ChatsProps
          open={show}
          onClose={handleClose}
          onConfirm={handleConfirm}
        />
      )}
    </div>
  );
};
