import React, { useState } from "react";
import { ChatsProps } from "./ChatsProps";

export const ConversationPlusButton = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  const handleConfirm = () => {
    setShow(false);
  };
  return (
    <div>
      <i onClick={handleShow} className="fa fa-plus"></i>
      {show && (
        <ChatsProps
          open={show}
          onClose={handleClose}
          onConfirm={handleConfirm}
        />
      )}
    </div>
  );
};
