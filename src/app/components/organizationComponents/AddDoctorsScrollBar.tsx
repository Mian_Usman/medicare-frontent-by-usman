import React from "react";
import { ChatsAddMemberPofileSections } from "../chatsComponents";

export const AddDoctorsScrollBar = () => {
  return (
    <div>
      <div className="doc-add-gruop-member-main-container">
        <div className="modal-scrollbar">
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
          <ChatsAddMemberPofileSections />
        </div>
      </div>
    </div>
  );
};
