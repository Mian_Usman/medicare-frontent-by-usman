import React from "react";
import { AddMemberSubComponent } from "./AddMemberSubComponent";

export const AddMemberSection = () => {
  return (
    <div>
      {" "}
      <div className="organization-gruop-member-main-container">
        <div className="modal-scrollbar">
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
          <AddMemberSubComponent />
        </div>
      </div>
    </div>
  );
};
