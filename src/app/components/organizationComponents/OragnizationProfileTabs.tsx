import React, { useState } from "react";
import { OrganzationCardsDetails } from "./OrganzationCardsDetails";
import profileImg from "../../asstes/gallery/1card-img.svg";

export const OragnizationProfileTabs = () => {
  const [index, setIndex] = useState(0);
  return (
    <div className="organzation-tabs-group-main-container">
      <div className="tabs-group-sub-container">
        <div className="organzation-tab-list-section ">
          <div className="tab-list">
            <div
              className="tabSections"
              onClick={() => {
                setIndex(0);
              }}
            >
              <h4>Doctors</h4>
            </div>
            <div
              className="tabSections"
              onClick={() => {
                setIndex(1);
              }}
            >
              <h4>Clinic</h4>
            </div>
            <div
              className="tabSections"
              onClick={() => {
                setIndex(2);
              }}
            >
              <h4>Managing Staff</h4>
            </div>
          </div>
          <div className="button">
            <button className="remove-button">Remove</button>
          </div>
          <div className="tab-sections">
            <div className="tab-section" hidden={index !== 0}>
              <div style={{ display: "flex", gap: "30px", flexWrap: "wrap" }}>
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
                <OrganzationCardsDetails
                  img={profileImg}
                  name="Hassan Adeel"
                  specialist="Physcial Therapy"
                />
              </div>
            </div>
            <div className="tab-section" hidden={index !== 1}>
              hello viewers how was the day? mention somthing...
            </div>
            <div className="tab-section" hidden={index !== 2}>
              How was the tab section? please rate the building
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
