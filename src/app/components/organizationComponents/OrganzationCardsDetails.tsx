import React from "react";
import sample from "../../asstes/gallery/1card-img.svg";

interface Prop {
  img: string;
  name: string;
  specialist: string;
}

export const OrganzationCardsDetails = (props: Prop) => {
  return (
    <>
      <div className="organization-card-main">
        {/* <div className="checkbox-organization">
          <input type="checkbox" name="chexkbox" id="checkbox" />
        </div> */}
        <div className="card-img-data">
          <div className="img-rounds">
            <img src={props.img} alt="" />
          </div>
          <h4>{props.name}</h4>
          <p>{props.specialist}</p>
        </div>
        <div className="buttons">
          <button className="btn1">Message</button>
          <button className="btn2">View Profile</button>
        </div>
      </div>
    </>
  );
};
