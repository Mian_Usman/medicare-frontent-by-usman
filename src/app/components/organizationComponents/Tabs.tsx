import React, { useState } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import { AddMemberSection } from "./AddMemberSection";
const img = require("../../asstes/gallery/settings-form-img.png");

export const OrganizationTabs = () => {
  const [index, setIndex] = useState(0);
  return (
    <div className="tabs-group-main-container">
      <div className="tabs-group-sub-container">
        <div className="tab-list-section">
          <div className="tab-list">
            <div
              className="tabSections"
              onClick={() => {
                setIndex(0);
              }}
            >
              <h4>Doctors</h4>
            </div>
            <div
              className="tabSections"
              onClick={() => {
                setIndex(1);
              }}
            >
              <h4>Clinic</h4>
            </div>
            <div
              className="tabSections"
              onClick={() => {
                setIndex(2);
              }}
            >
              <h4>Managing Staff</h4>
            </div>
          </div>
          <div className="tab-sections">
            <div className="tab-section" hidden={index !== 0}>
              <AddMemberSection />
            </div>
            <div className="tab-section" hidden={index !== 1}>
              hello viewers how was the day? mention somthing...
            </div>
            <div className="tab-section" hidden={index !== 2}>
              How was the tab section? please rate the building
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
