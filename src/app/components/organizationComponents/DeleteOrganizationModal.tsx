import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { useDispatch } from "../../../API/store";
import {
  OrganizationDeleteThunk,
  OrganizationListThunk,
} from "../../../API/thunk";

interface Props {
  id: string;
  open: boolean;
  onClose: () => void;
}
export const DeleteOrganizationModal = (props: Props) => {
  const dispatch = useDispatch();

  const handleConfirmDelete = () => {
    dispatch(OrganizationDeleteThunk(props.id));
    props.onClose();
    dispatch(OrganizationListThunk());
  };
  useEffect(() => {
    dispatch(OrganizationListThunk());
  }, []);

  return (
    <Modal
      show={props.open}
      onClose={props.onClose}
      className="modal-main-container"
    >
      <div className="delete-button-modal-main-container">
        <div className="properties">
          <p>Are you sure you want to delete this clinic?</p>
          <div className="buttons">
            <button className="empty-light-button" onClick={props.onClose}>
              Cancel
            </button>
            <button className="solid-button" onClick={handleConfirmDelete}>
              Delete
            </button>
          </div>
        </div>
      </div>
    </Modal>
  );
};
