import Modal from "react-bootstrap/Modal";
import { ChatModalAddGroupMember } from "../chatsComponents/ChatModalAddGroupMember";
import { AddDoctorsScrollBar } from "./AddDoctorsScrollBar";

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

export const AddDoctorsModal = (props: Props) => {
  return (
    <Modal
      show={props.open}
      onHide={props.onClose}
      className="modal-main-container"
    >
      <div className="organization-modals-container">
        <div className="modals-inner">
          <h3>Add Doctors</h3>
        </div>
        <div className="doctors-search-input">
          <input type="search" name="search" id="search" placeholder="Search" />
        </div>
        <div>
          <AddDoctorsScrollBar />
        </div>

        <div
          className="modals-buttons-width"
          style={{
            display: "flex",
            justifyContent: "flex-end",
            width: "435px",
            marginTop: "10px",
            padding: "10px 0",
          }}
        >
          <button
            onClick={props.onClose}
            className="solid-button"
            style={{ padding: "5px 20px" }}
          >
            Add Doctors
          </button>
        </div>
      </div>
    </Modal>
  );
};
