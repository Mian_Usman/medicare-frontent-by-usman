import { useFormik } from "formik";
import Modal from "react-bootstrap/Modal";
import { API } from "../../../API/api";
import { useDispatch } from "../../../API/store";
import { OrganizationListThunk } from "../../../API/thunk";
import { FormikValidator } from "../../../shared/utility";
import { CreateNewOrganzationModalValidator } from "../../../shared/validators/CreateNewOrganizationModal";
import { FormikErrorMessage } from "../FormikErrorMessage";

interface Props {
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

export const CreateNewOrganizationModal = (props: Props) => {
  const dispatch = useDispatch();

  const CreateNewOrganizationFormik =
    useFormik<CreateNewOrganzationModalValidator>({
      initialValues: {
        name: "",
        description: "",
      },
      onSubmit: (values) => {
        API.Organization.CreateNewOrganization(values)
          .then((response) => {
            console.log(response);
            props.onClose();
            dispatch(OrganizationListThunk());
          })
          .catch((error) => {
            console.log(error);
          });
      },

      validate: FormikValidator.validator(CreateNewOrganzationModalValidator),
    });

  return (
    <Modal
      show={props.open}
      onHide={props.onClose}
      className="modal-main-container"
    >
      <form onSubmit={CreateNewOrganizationFormik.handleSubmit}>
        <div className="staff-organization-modals-container">
          <div className="modals-inner">
            <h3>Create An Organization</h3>
          </div>
          <div className="modals-inputs">
            <div className="inputs">
              <label htmlFor="">Name</label>
              <input
                type="text"
                name="name"
                id="name"
                placeholder="Name"
                onChange={CreateNewOrganizationFormik.handleChange}
                onBlur={CreateNewOrganizationFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={CreateNewOrganizationFormik}
                name="name"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Description </label>
              <input
                type="text"
                name="description"
                id="description"
                placeholder="description "
                onChange={CreateNewOrganizationFormik.handleChange}
                onBlur={CreateNewOrganizationFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={CreateNewOrganizationFormik}
                name="description"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
          </div>

          <div
            className="modals-buttons-width"
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: "450px",
              marginTop: "10px",
              padding: "10px 0",
            }}
          >
            <button
              type="submit"
              className="solid-button"
              style={{ padding: "5px 20px" }}
            >
              Add Staff
            </button>
          </div>
        </div>
      </form>
    </Modal>
  );
};
