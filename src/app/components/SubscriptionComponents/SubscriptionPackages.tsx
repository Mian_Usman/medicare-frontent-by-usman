import React from "react";

export const SubscriptionPackages = () => {
  return (
    <div className="main-container-subscription-packages">
      <div className="packages">
        <div className="basic-price-details">
          <h3>Basics</h3>
          <div className="price">
            <h3>
              $10.53 <span>/Month</span>
            </h3>
          </div>
        </div>
        <div className="para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni id
          magnam quis blanditiis ad cupiditate itaque hic velit animi, nemo
          assumenda sint distinctio aliquam quam ea corporis deserunt nisi modi
          quo suscipit doloremque autem. Voluptatem quidem iusto nihil doloribus
          aspernatur!
        </div>
        <button className="solid-button">Get Started</button>
      </div>
      <div className="packages">
        <div className="basic-price-details">
          <h3>Silver</h3>
          <div className="price">
            <h3>
              $20.99 <span>/Month</span>
            </h3>
          </div>
        </div>
        <div className="para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni id
          magnam quis blanditiis ad cupiditate itaque hic velit animi, nemo
          assumenda sint distinctio aliquam quam ea corporis deserunt nisi modi
          quo suscipit doloremque autem. Voluptatem quidem iusto nihil doloribus
          aspernatur!
        </div>
        <button className="solid-button">Get Started</button>
      </div>
      <div className="packages">
        <div className="basic-price-details">
          <h3>Gold</h3>
          <div className="price">
            <h3>
              $40.50 <span>/Month</span>
            </h3>
          </div>
        </div>
        <div className="para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni id
          magnam quis blanditiis ad cupiditate itaque hic velit animi, nemo
          assumenda sint distinctio aliquam quam ea corporis deserunt nisi modi
          quo suscipit doloremque autem. Voluptatem quidem iusto nihil doloribus
          aspernatur!
        </div>
        <button className="solid-button">Get Started</button>
      </div>
    </div>
  );
};
