import React from "react";

interface Props {
  profile: string;
  name: string;
  para: string;
  date: string;
  newMsg: string;
}
export const ConversationProfileViews = (props: Props) => {
  return (
    <div className="conversation-profile-container">
      <div className="profile-container">
        <div className="img-name-flex">
          <div className="conversation-round-images">
            <img src={props.profile} alt="" />
            <i className="fa fa-cicle online-dot"></i>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div>
              <div className="name-para">
                <h4>{props.name} </h4>
                <p>{props.para}</p>
              </div>
            </div>
            <div className="date-msg-flex">
              <div className="date-new-msg">
                <p>{props.date}</p>
                <button className="new-msgs">{props.newMsg}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
