import React from "react";
import { Dropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

interface Props {
  number: string;
  clinicDetails: string;
  clinicAdmin: string;
  city: any;
  clinicEmail: string;
  clinicPhoneNumber: string;
}

interface secProps {
  number: string;
  clinicDetails: string;
  clinicAdmin: string;
  clinicEmail: string;
  clinicPhoneNumber: string;
  buttons: any;
  clinicDots: any;
}

interface thirdProps {
  organization: string;
  description: string;
  doctor: string;
  clinics: string;
  others: string;
  clinicDots: any;
}

export const ClinicTableList = (props: Props) => {
  return (
    <tr>
      <td data-label="Number">{props.number}</td>
      <td data-label="Clinic Details">{props.clinicDetails}</td>
      <td data-label="Clinic Admin">{props.clinicAdmin}</td>
      <td data-label="City">{props.city}</td>
      <td data-label="Clinic Email">{props.clinicEmail}</td>
      <td data-label="Clinic Phone Number">{props.clinicPhoneNumber}</td>
    </tr>
  );
};

export const SubscriptionTableList = (propis: secProps) => {
  return (
    <tr>
      <td data-label="Number">{propis.number}</td>
      <td data-label="Clinic Details">{propis.clinicDetails}</td>
      <td data-label="Clinic Admin">{propis.clinicAdmin}</td>
      <td data-label="Clinic Email">{propis.clinicEmail}</td>
      <td data-label="Clinic Phone Number">{propis.clinicPhoneNumber}</td>
      <td data-label="buttons">{propis.buttons}</td>
      <td data-label="Clinic Dots" style={{ cursor: "pointer" }}>
        {propis.clinicDots}
      </td>
    </tr>
  );
};

export const OrganizationTableList = (propes: thirdProps) => {
  return (
    <tr>
      <NavLink to={"/details-organization-data"}>
        <td data-label="Organization">{propes.organization}</td>
      </NavLink>
      <td data-label="Doctor">{propes.description}</td>
      <td data-label="Clinics">{propes.doctor}</td>
      <td data-label="description">{propes.clinics}</td>
      <td data-label="Others">{propes.others}</td>
      <td data-label="Clinic Dots" style={{ cursor: "pointer" }}>
        {propes.clinicDots}
      </td>
    </tr>
  );
};
