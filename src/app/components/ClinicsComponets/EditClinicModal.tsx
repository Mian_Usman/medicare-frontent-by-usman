import { useFormik } from "formik";
import { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { API } from "../../../API/api";
import { useDispatch, useSelector } from "../../../API/store";
import {
  ClinicsListThunk,
  DeleteClinicsThunk,
  OrganizationListThunk,
} from "../../../API/thunk";
import { AddNewClinicInterface } from "../../../shared/interfaces";
import { FormikValidator } from "../../../shared/utility";
import { EditClinicValidator } from "../../../shared/validators/EditClinicValidator";
import { FormikErrorMessage } from "../FormikErrorMessage";

interface Props {
  clinic: AddNewClinicInterface;
  openEditor: boolean;
  closeEditor: () => void;
}

export const EditClinicModal = (props: Props) => {
  const clinic = props.clinic;
  const dispatch = useDispatch();

  const organization = useSelector((state) => state.organization);
  useEffect(() => {
    dispatch(OrganizationListThunk());
  }, [dispatch]);

  const EditClinicFormik = useFormik<EditClinicValidator>({
    initialValues: {
      // name: "",
      // email: "",
      // phoneNumber: "",
      // address1: "",
      // address2: "",
      // plan: "",
      // organization: "",
      name: clinic.name,
      email: clinic.email,
      phoneNumber: clinic.phoneNumber,
      address1: clinic.address1,
      address2: clinic.address2,
      plan: clinic.plan,
      organization: clinic.organization,
    },
    onSubmit: (values) => {
      API.Clinics.updateClinicById(clinic.id, values)
        .then((response) => {
          console.log(response);
          props.closeEditor();
          dispatch(ClinicsListThunk());
        })
        .catch((error) => {
          console.log(error);
        });
      console.log(values);
    },

    validate: FormikValidator.validator(EditClinicValidator),
  });

  //   const thisis = () => console.log("first priority");

  useEffect(() => {
    console.log();
  }, []);

  return (
    <Modal
      show={props.openEditor}
      onHide={props.closeEditor}
      className="modal-main-container"
    >
      <div className="staff-organization-modals-container">
        <div className="modals-inner">
          <h3>Edit Clinic</h3>
        </div>
        <form onSubmit={EditClinicFormik.handleSubmit}>
          <div className="modals-inputs">
            <div className="inputs">
              <label htmlFor="">Name</label>
              <input
                type="text"
                name="name"
                value={EditClinicFormik.values.name}
                id="name"
                placeholder="Name"
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="name"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Email </label>
              <input
                type="email"
                name="email"
                id="email"
                value={EditClinicFormik.values.email}
                placeholder="Email "
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="email"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Phone </label>
              <input
                type="number"
                name="phoneNumber"
                id="phone number"
                value={EditClinicFormik.values.phoneNumber}
                placeholder="Phone Number "
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="phoneNumber"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Clinic Address Line 1</label>
              <input
                type="text"
                name="address1"
                id="address1"
                value={EditClinicFormik.values.address1}
                placeholder="address1"
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="address1"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Clinic Address Line 2</label>
              <input
                type="text"
                name="address2"
                id="address2"
                value={EditClinicFormik.values.address2}
                placeholder="Address"
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="address2"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Subscription Plan</label>
              <input
                type="text"
                name="plan"
                id="plan"
                value={EditClinicFormik.values.plan}
                placeholder="Plan"
                onChange={EditClinicFormik.handleChange}
                onBlur={EditClinicFormik.handleBlur}
              />
              <FormikErrorMessage
                formik={EditClinicFormik}
                name="plan"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
            <div className="inputs">
              <label htmlFor="">Organization</label>

              <select
                style={{ padding: "10px" }}
                defaultValue={"DEFAULT"}
                className="select-input options-inputs"
                name="organization"
                onChange={EditClinicFormik.handleChange}
              >
                <option value={null} selected={true} disabled>
                  Select
                </option>
                {organization.data.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>

              <FormikErrorMessage
                formik={EditClinicFormik}
                name="organization"
                render={(error) => <span className="error">{error}</span>}
              />
            </div>
          </div>

          <div
            className="modals-buttons-width"
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: "450px",
              marginTop: "10px",
              padding: "10px 0",
            }}
          >
            <button
              type="submit"
              className="solid-button"
              style={{ padding: "5px 20px" }}
            >
              Save Changes
            </button>
          </div>
        </form>
      </div>
    </Modal>
  );
};
