import React from "react";
import { useAuthContext } from "../../shared/contexts";
import { HeaderDropdown } from "./HeaderDropdown";
const headerProfileImage = require("../asstes/gallery/headerprofileimage.png");

export const Header = () => {
  const context = useAuthContext();
  return (
    <div className="main-header">
      <div className="main-header-container">
        <div></div>
        <div className="header-elements">
          <div className="header-notification">
            <i className="fa fa-bell">
              <span>
                <i className="fa fa-circle"></i>
              </span>
            </i>
          </div>
          <div className="header-profile">
            <img src={headerProfileImage} alt="" />
            <HeaderDropdown help="help" settings="settings" logout="logout" />
          </div>
        </div>
      </div>
    </div>
  );
};
