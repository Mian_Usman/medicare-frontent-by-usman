import React from "react";
const profileImg = require("../asstes/gallery/sub-profile-img.png");

interface Props {
  name: string;
  desgination: string;
  requestsPayments: string;
}

export const SubscriptionFee = (props: Props) => {
  return (
    <div className="subscription-fee-list">
      <div className="profile-info">
        <img src={profileImg} alt="" />
        <div className="name-designation">
          <div className="h3">{props.name}</div>
          <p>{props.desgination}</p>
        </div>
      </div>
      <div className="request-name-designation">
        <li>{props.requestsPayments}</li>
      </div>
    </div>
  );
};
