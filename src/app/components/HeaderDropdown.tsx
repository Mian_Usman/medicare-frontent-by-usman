import { Dropdown } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../../shared/contexts";
import dropDown from "../asstes/gallery/1dropdownHeader.svg";

interface Props {
  help: string;
  settings: string;
  logout: string;
}

export const HeaderDropdown = (props: Props) => {
  const context = useAuthContext();
  const navigate = useNavigate();

  const handleLogout = () => {
    context.updateUser(null);
    navigate("login");
  };
  return (
    <Dropdown className="header-dropdown-main">
      <Dropdown.Toggle className="header-dropdown">
        <img src={dropDown} alt="" className="transparents12" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="header-dropdown-menu">
        <Dropdown.Item className="header-dropdown-item" href="#/action-1">
          {props.help}
        </Dropdown.Item>
        <Dropdown.Item className="header-dropdown-item" href="#/action-2">
          {props.settings}
        </Dropdown.Item>
        <Dropdown.Item className="header-dropdown-item" onClick={handleLogout}>
          {props.logout}
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};
