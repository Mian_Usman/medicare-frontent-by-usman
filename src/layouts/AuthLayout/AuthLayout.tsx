import React from "react";
import { Outlet } from "react-router-dom";
import { Login } from "../../app/views";

export const AuthLayout = () => {
  return <Outlet />;
};
