import React from "react";
import { Outlet } from "react-router-dom";
import { Header, MainNavbar } from "../../app/components";
import { SubNavbar } from "../../app/components/SubAdminComponents";

export const MainLayout = () => {
  return (
    <div style={{ display: "flex", width: "100%", height: "100%" }}>
      <MainNavbar />
      {/* <SubNavbar /> */}
      <div className="layout-container">
        <Header />
        <div className="layout-background-color">
          <Outlet />
        </div>
      </div>
    </div>
  );
};
